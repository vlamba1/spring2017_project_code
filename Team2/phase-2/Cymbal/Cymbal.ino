#include <Wire.h>

byte wireRead;
void setup() {
  // put your setup code here, to run once:
  //begin wire communication and look for signal sent across wire
  Wire.begin(8);
  Wire.onReceive(receiveEvent);
  Serial.begin(9600);
  //set PWM pins to output and set motors to upright position
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  analogWrite(10, 0);
  analogWrite(9, 255);
}

void loop() {
  // put your main code here, to run repeatedly:
  //activate sequence one
  if(wireRead == 49) {
    wireRead = 0;
    sequenceOne();
  //activate sequence two
  } else if(wireRead == 50) {
    wireRead = 0;
    sequenceTwo();
  //activate sequence three
  } else if(wireRead == 51) {
    wireRead = 0;
    sequenceThree();
  //activate sequence four
  } else if(wireRead == 52) {
    wireRead = 0;
    sequenceFour();
  //activate sequence five
  } else if(wireRead == 53) {
    wireRead = 0;
    sequenceFive();
  //activate sequence six
  } else if(wireRead == 54) {
    wireRead = 0;
    sequenceSix();
  } else if(wireRead == 55) {
    wireRead = 0;
    hitQuarter();
  }
}
//function called upon wire receive event
void receiveEvent(int howMany) {

    wireRead = Wire.read();
    
}

// function to hit drum for eighth note
void hitEighth() {
  analogWrite(10, 255);
  analogWrite(9, 0);
  delay(100);
  analogWrite(9, 255);
  analogWrite(10, 0);
  delay(25);
}
//function to hit drum for quarter note
void hitQuarter() {
  analogWrite(10, 255);
  analogWrite(9, 0);
  delay(100);
  analogWrite(9, 255);
  analogWrite(10, 0);
  delay(150);
}
//function to hit drum for half note
void hitHalf() {
  analogWrite(10, 255);
  analogWrite(9, 0);
  delay(100);
  analogWrite(9, 255);
  analogWrite(10, 0);
  delay(400);
}
//sequence one function
void sequenceOne() {
  delay(4000);
  hitQuarter();
  delay(500);
  hitQuarter();
  hitQuarter();
  delay(500);
  hitQuarter();
  hitQuarter();
  delay(500);
  hitQuarter();
  hitQuarter();
  delay(500);
  hitQuarter();  
  hitQuarter();
  delay(500);
  hitQuarter();
  hitQuarter();
  delay(500);
  hitQuarter();
}
//sequence two function
void sequenceTwo() {
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
}
//sequence three function
void sequenceThree() {
  delay(1000);
  hitQuarter();
  delay(250);
  hitQuarter();
  delay(250);
  delay(1000);
  hitQuarter();
  delay(250);
  hitQuarter();
  delay(250);
  delay(6000);
}
//sequence four function
void sequenceFour() {
  delay(1000);
  hitQuarter();
  delay(500);
  hitQuarter();
  delay(1000);
  hitQuarter();
  delay(500);
  hitQuarter();
  delay(3000);
  hitQuarter();
  delay(500);
  hitQuarter();
  delay(1000);
  hitQuarter();
  delay(500);
  hitQuarter();
}
//sequence five function
void sequenceFive() {
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
}
//sequence six function
void sequenceSix() {
  delay(250);
  hitQuarter();
  delay(250);
  hitQuarter();
  delay(250);
  hitQuarter();
  delay(250);
  hitQuarter();
  delay(250);
  hitQuarter();
  delay(250);
  hitQuarter();
  delay(250);
  hitQuarter();
  delay(250);
  hitQuarter();
  delay(250);
  hitQuarter();
  delay(250);
  hitQuarter();
  delay(250);
  hitQuarter();
  delay(250);
  hitQuarter();
  delay(250);
  hitQuarter();
  delay(250);
  hitQuarter();
  delay(250);
  hitQuarter();
  delay(250);
  hitQuarter();
  delay(250);
  hitQuarter();
  delay(250);
  hitQuarter();
}


#include <Wire.h>

byte wireRead;

void setup() {
  // put your setup code here, to run once:
  //begin wire communication and look for signal sent across wire
  Wire.begin(8);
  Wire.onReceive(receiveEvent);
  Serial.begin(9600);
  //set PWM pins to output and set motors to upright position
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  analogWrite(6, 0);
  analogWrite(5, 255);
  analogWrite(10, 0);
  analogWrite(9, 255);
}


void loop() {
  // put your main code here, to run repeatedly:
  //activate sequence one
  if(wireRead == 49) {
    wireRead = 0;
    sequenceOne();
  //activate sequence two
  } else if(wireRead == 50) {
    wireRead = 0;
    sequenceTwo();
  //activate sequence three
  } else if(wireRead == 51) {
    wireRead = 0;
    sequenceThree();
  //activate sequence four
  } else if(wireRead == 52) {
    wireRead = 0;
    sequenceFour();
  //activate sequence five
  } else if(wireRead == 53) {
    wireRead = 0;
    sequenceFive();
  //activate sequence six
  } else if(wireRead == 54) {
    wireRead = 0;
    sequenceSix();
  //activate test hit
  } else if(wireRead == 55) {
    wireRead = 0;
    hitBothQuarter();
  }
}
//function called upon wire receive event
void receiveEvent(int howMany) {

    wireRead = Wire.read();
    
}

// function to hit drum with left stick for quarter note
void hitLeftQuarter() {
  analogWrite(6, 255);
  analogWrite(5, 0);
  delay(100);
  analogWrite(6, 0);
  analogWrite(5, 255);
  delay(150);
}
//function to hit drum with left stick for eighth note
void hitLeftEighth() {
  analogWrite(6, 255);
  analogWrite(5, 0);
  delay(100);
  analogWrite(6, 0);
  analogWrite(5, 255);
  delay(25);
}
//function to hit drum with left stick for half note
void hitLeftHalf() {
  analogWrite(6, 255);
  analogWrite(5, 0);
  delay(100);
  analogWrite(6, 0);
  analogWrite(5, 255);
  delay(400);
}

// function to hit drum with right stick for quarter note
void hitRightQuarter() {
  analogWrite(10, 255);
  analogWrite(9, 0);
  delay(100);
  analogWrite(10, 0);
  analogWrite(9, 255);
  delay(150);
}
//function to hit drum with right stick for eighth note
void hitRightEighth() {
  analogWrite(10, 255);
  analogWrite(9, 0);
  delay(100);
  analogWrite(10, 0);
  analogWrite(9, 255);
  delay(25);
}
//function to hit drum with right stick for half note
void hitRightHalf() {
  analogWrite(10, 255);
  analogWrite(9, 0);
  delay(100);
  analogWrite(10, 0);
  analogWrite(9, 255);
  delay(400);
}
// function to hit drum with both sticks for quarter note
void hitBothQuarter() {
  analogWrite(6, 255);
  analogWrite(5, 0);
  analogWrite(10, 255);
  analogWrite(9, 0);
  delay(100);
  analogWrite(6, 0);
  analogWrite(5, 255);
  analogWrite(10, 0);
  analogWrite(9, 255);
  delay(150);
}
//function to hit drum with both sticks for eighth note
void hitBothEighth() {
  analogWrite(6, 255);
  analogWrite(5, 0);
  analogWrite(10, 255);
  analogWrite(9, 0);
  delay(100);
  analogWrite(6, 0);
  analogWrite(5, 255);
  analogWrite(10, 0);
  analogWrite(9, 255);
  delay(25);
}
//function to hit drum with both sticks for half note
void hitBothHalf() {
  analogWrite(6, 255);
  analogWrite(5, 0);
  analogWrite(10, 255);
  analogWrite(9, 0);
  delay(100);
  analogWrite(6, 0);
  analogWrite(5, 255);
  analogWrite(10, 0);
  analogWrite(9, 255);
  delay(400);
}
//sequence one function
void sequenceOne() {
  delay(6000);
  delay(250);
  hitLeftQuarter();
  delay(250);
  hitRightQuarter();
  delay(250);
  hitLeftQuarter();
  delay(250);
  hitRightQuarter();
  delay(250);
  hitLeftQuarter();
  delay(250);
  hitRightQuarter();
  delay(1000);
}
//sequence two function
void sequenceTwo() {
  delay(6000);
  delay(250);
  hitBothQuarter();
  hitBothQuarter();
  delay(250);
  hitBothQuarter();
  delay(750);
  hitBothQuarter();
  delay(750);
  delay(500);
  hitBothQuarter();
  hitBothQuarter();
}
//sequence three function
void sequenceThree() {
  delay(6000);
  hitLeftQuarter();
  hitRightQuarter();
  hitLeftQuarter();
  hitRightQuarter();
  delay(1000);
  hitBothHalf();
  hitBothHalf();
  delay(1000);
}
//sequence four function
void sequenceFour() {
  delay(1000);
  delay(500);
  hitLeftEighth();
  hitRightEighth();
  hitLeftEighth();
  delay(125);
  delay(5000);
  hitLeftEighth();
  hitRightEighth();
  hitLeftEighth();
  delay(125);
  delay(2000);  
}
//sequence five function
void sequenceFive() {
  delay(500);
  hitBothQuarter();
  hitBothQuarter();
  delay(1000);
  delay(500);
  hitBothQuarter();
  hitBothQuarter();
  delay(1000);
  delay(500);
  hitBothQuarter();
  hitBothQuarter();
  delay(1000);
  delay(500);
  hitBothQuarter();
  hitBothQuarter();
  delay(1000);
  delay(500);
  hitBothQuarter();
  hitBothQuarter();
  delay(1000);
}
//sequence six function
void sequenceSix() {
  delay(9000);
  hitBothQuarter();
  hitBothQuarter();
  hitBothQuarter();
  hitBothQuarter();
}


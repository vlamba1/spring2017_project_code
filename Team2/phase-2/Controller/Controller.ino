#include <Wire.h>

byte byteRead;
void setup() {
  // put your setup code here, to run once:
  //initialize wire transmission
  Wire.begin();
  Serial.begin(9600);
}

void loop() {
  // put your main code here, to run repeatedly:
  //check if serial monitor is open and ready
  if(Serial.available()) {
    //read in byte entered to serial monitor and write to wire
    byteRead = Serial.read();
    Serial.write(byteRead);
    Wire.beginTransmission(8);
    Wire.write(byteRead);
    delay(10);
    Wire.endTransmission();
    //confirms transmission ended
    Serial.println("Done");
    delay(0);
  }

}

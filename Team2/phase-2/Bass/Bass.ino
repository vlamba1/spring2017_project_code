#include <Wire.h>

byte wireRead;
void setup() {
  // put your setup code here, to run once:
  //begin wire communication and look for signal sent across wire
  Wire.begin(8);
  Wire.onReceive(receiveEvent);
  Serial.begin(9600);
  //set PWM pins to output and set motor to upright position
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  analogWrite(10, 0);
  analogWrite(9, 255);
}

void loop() {
  // put your main code here, to run repeatedly:
  //activate sequence 1
  if(wireRead == 49) {
    wireRead = 0;
    sequenceOne();
  //activate sequence 2
  } else if(wireRead == 50) {
    wireRead = 0;
    sequenceTwo();
  //activate sequence 3
  } else if(wireRead == 51) {
    wireRead = 0;
    sequenceThree();
  //activate sequence 4
  } else if(wireRead == 52) {
    wireRead = 0;
    sequenceFour();
  //activate sequence 5
  } else if(wireRead == 53) {
    wireRead = 0;
    sequenceFive();
  //actiavte sequence 6
  } else if(wireRead == 54) {
    wireRead = 0;
    sequenceSix();
  } else if(wireRead == 55) {
    wireRead = 0;
    hitQuarter();
  }
  
}

//function called upon wire receive event
void receiveEvent(int howMany) {

    wireRead = Wire.read();
    Serial.println(wireRead);
}

// function to hit drum with stick for eighth note
void hitEighth() {
  analogWrite(10, 255);
  analogWrite(9, 0);
  delay(100);
  analogWrite(9, 255);
  analogWrite(10, 0);
  delay(25);
}
// function to hit drum with stick for quarter note
void hitQuarter() {
  analogWrite(10, 255);
  analogWrite(9, 0);
  delay(100);
  analogWrite(9, 255);
  analogWrite(10, 0);
  delay(150);
}
// function to hit drum with stick for half note
void hitHalf() {
  analogWrite(10, 255);
  analogWrite(9, 0);
  delay(100);
  analogWrite(9, 255);
  analogWrite(10, 0);
  delay(400);
}
//sequence one function
void sequenceOne() {
  hitHalf();
  hitHalf(); 
  hitHalf();
  hitHalf();
  hitHalf();
  hitHalf();  
  hitHalf();
  hitHalf(); 
  hitHalf();
  hitHalf();  
  hitHalf();
  hitHalf();  
  hitHalf();
  hitHalf();  
  hitHalf();
  hitHalf();  
  hitHalf();
  hitHalf();  
  hitHalf();
  hitHalf();
}
//sequence two function
void sequenceTwo() {
  delay(4000);
  hitEighth();
  hitEighth();
  delay(1000);
  hitEighth();
  hitEighth();
  delay(125);
  hitEighth();
  hitEighth();
  delay(125);
  hitEighth();
  hitEighth();
  delay(1000);
  hitEighth();
  hitEighth();
  delay(125);
  hitEighth();
  hitEighth();
  delay(125);
}
//sequence three function
void sequenceThree() {
  hitQuarter();
  delay(750);
  hitQuarter();
  delay(750);
  hitQuarter();
  delay(750);
  hitQuarter();
  delay(750);
  hitQuarter();
  delay(750);
  hitQuarter();
  delay(750);
  hitQuarter();
  delay(750);
  hitQuarter();
  delay(750);
  hitQuarter();
  delay(750);
  hitQuarter();
  delay(750);
}
//sequence four function
void sequenceFour() {
  hitEighth();
  delay(125);
  hitEighth();
  hitEighth();
  delay(500);
  hitEighth();
  delay(125);
  hitEighth();
  hitEighth();
  delay(500);
  hitEighth();
  delay(125);
  hitEighth();
  hitEighth();
  delay(500);
  hitEighth();
  delay(125);
  hitEighth();
  hitEighth();
  delay(500);
  hitEighth();
  delay(125);
  hitEighth();
  hitEighth();
  delay(500);
  hitEighth();
  delay(125);
  hitEighth();
  hitEighth();
  delay(500);
  hitEighth();
  delay(125);
  hitEighth();
  hitEighth();
  delay(500);
  hitEighth();
  delay(125);
  hitEighth();
  hitEighth();
  delay(500);
  hitEighth();
  delay(125);
  hitEighth();
  hitEighth();
  delay(500);
  hitQuarter();
  hitQuarter();
  hitQuarter();
  hitQuarter();
}
//sequence five function
void sequenceFive() {
  hitQuarter();
  delay(750);
  hitQuarter();
  delay(750);
  hitQuarter();
  delay(750);
  hitQuarter();
  delay(750);
  hitQuarter();
  delay(750);
  hitQuarter();
  delay(750);
  hitQuarter();
  delay(750);
  hitQuarter();
  delay(750);
  hitQuarter();
  delay(750);
  hitQuarter();
  delay(750);
}
//sequence six function
void sequenceSix() {
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
  delay(500);
  hitQuarter();
  delay(250);
}


int IN1 = 8; 		// LEFT STICK
int IN2 = 7; 		// LEFT STICK
int IN3 = 10; 		// RIGHT STICK
int IN4 = 11; 		// RIGHT STICK
//double x = 328.6;	//delay
//double y = 328.6;	//delay
double x = 400;
double y = 700;

void setup() {
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
}

void loop() {
  // Push the motor
  
  
  //Add to all, syncronization
	rightStickAttack();
	delay((1 * x));
	rightStickAttack();
	delay((1 * x));
	rightStickAttack();
	delay((1 * x));
	rightStickAttack();
	delay((1 * x));
 bothStickAttack();

	delay(100); //Just to space it out a bit

  
//Bohemian Rhapsody 46-54
	for(int i = 0; i < 8; i++){
	//Base on 0, 1.5, and 2
	//Assuming rightStickAttack
		rightStickAttack();
		delay((x * 1));
		delay((x * .5));
		rightStickAttack();
		delay((x*.5));
		rightStickAttack();
		delay((x * 1));
		delay((x * 1));
	}
//Bohemian Rhapsody 55
	rightStickAttack();
	delay((x * 4));


	rightStickAttack();
	delay((1 * x));
	rightStickAttack();
	delay((1 * x));
	rightStickAttack();
	delay((1 * x));
	rightStickAttack();
	delay((1 * x));
	
	delay(100); //Just to space it out a bit

//Base 
	for(int i = 0; i < 5; i++){
		delay((.5*y));  

		rightStickAttack();
		delay((.25*y));
		
		delay((.5*y));
		
		rightStickAttack();
		delay((.25*y));
		
		delay((.5*y));

		rightStickAttack();
		delay((.25*y));

		delay((.5*y));

		rightStickAttack();
		delay((.25*y));

		rightStickAttack();
		delay((.25*y));

		rightStickAttack();
		delay((.25*y));

		delay((.25*y));

		rightStickAttack();
		delay((.25*y));
	}

//Base
	for(int i = 0; i < 4; i++){
		rightStickAttack();
		delay((.5*y));
		rightStickAttack();
		delay((.25*y));
		rightStickAttack();
		delay((.25*y));
		delay((1*y));
		rightStickAttack();
		delay((2*y));
	}

  
}


void leftStickAttack() {
  // Left Down
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  delay(100);
  // Left Up
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  delay(75);
}

void rightStickAttack() {
  //Right Down
  digitalWrite(IN4, HIGH);
  digitalWrite(IN3, LOW);
  delay(100);
  //Right Up
  digitalWrite(IN4, LOW);
  digitalWrite(IN3, HIGH);
  delay(75);
}

void bothStickAttack(){
  //Down
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  digitalWrite(IN4, HIGH);
  digitalWrite(IN3, LOW);
  delay(100);
  //Up
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  digitalWrite(IN4, LOW);
  digitalWrite(IN3, HIGH);
  delay(75);
}

void quarterNotes(int numberOfHits){
  int i;
  for(i=0; i < numberOfHits; i++){
     leftStickAttack();
     delay(428.6); //One hit at 140 bpm
  }
}
void eigthNotes(int numberOfHits){
  int i;
  for(i=0; i < numberOfHits; i++){
     leftStickAttack();
     delay(214.3); //One hit at 140 bpm
  }
}

void doted8And16(int numberOfHits){
  int i;
  for(i=0; i<numberOfHits; i++){
    leftStickAttack();
    delay(321.45);
    rightStickAttack();
    delay(107.15);
  }
}





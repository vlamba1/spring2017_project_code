int IN1 = 7; // LEFT STICK
int IN2 = 8; // LEFT STICK
int IN3 = 10; // RIGHT STICK
int IN4 = 11; // RIGHT STICK


int interval = 100; // Seconds
// HIGH, LOW = PUSH (IN1, IN2)
// LOW, HIGH = PULL (IN1, IN2)
void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  //digitalWrite(IN2, LOW);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);

  
}

void loop() {
  // put your main code here, to run repeatedly:
  // Push the motor
  if(Serial.read() == 'k'){
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  digitalWrite(IN4, HIGH);
  digitalWrite(IN3, LOW);
  delay(200);
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  digitalWrite(IN4, LOW);
  digitalWrite(IN3, HIGH);
  delay(100);
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  delay(200);
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  delay(100);
  digitalWrite(IN4, HIGH);
  digitalWrite(IN3, LOW);
  delay(200);
  digitalWrite(IN4, LOW);
  }
   if(Serial.read() == 'j'){
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  delay(200);
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  delay(100);
  digitalWrite(IN4, HIGH);
  digitalWrite(IN3, LOW);
  delay(200);
  digitalWrite(IN4, LOW);
  digitalWrite(IN3, HIGH);
  }
  if(Serial.read() == 'l'){
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  delay(200);
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  delay(100);
  digitalWrite(IN4, HIGH);
  digitalWrite(IN3, LOW);
  delay(200);
  digitalWrite(IN4, LOW);
  digitalWrite(IN3, HIGH);
  }
  if (Serial.read() == 'a'){
    int i;
    for(i = 0; i <5; i++){
      leftStickAttack();
      rightStickAttack();
      bothStickAttack();
      bothStickAttack();
      bothStickAttack();
      leftStickAttack();
      leftStickAttack();
      bothStickAttack();
      rightStickAttack();
      rightStickAttack();
      delay(100);
      leftStickAttack();
      rightStickAttack();
      leftStickAttack();
      leftStickAttack();
      rightStickAttack();
      delay(750);
      bothStickAttack();
      rightStickAttack();
      rightStickAttack();
      rightStickAttack();
      bothStickAttack();
      bothStickAttack();
      leftStickAttack();
      leftStickAttack();
      leftStickAttack();
      delay(500);
      leftStickAttack();
      delay(300);
      rightStickAttack();
      delay(300);
      leftStickAttack();
      delay(300);
      bothStickAttack();
      delay(1000);
      
    }
  }
   if (Serial.read() == 'd'){
    int i;
    int k;
    for(i = 0; i <2; i++){
     for (k = 0; k < 85; k++) {
      leftStickAttack();
      delay(100);
     }
     delay(3000);;
     rightStickAttack();
     bothStickAttack();
    }
  }

   if (Serial.read() == 's'){
    int i;
    for(i = 0; i <4; i++){
      leftStickAttack();
      rightStickAttack();
      leftStickAttack();
      rightStickAttack();
      leftStickAttack();
      rightStickAttack();
      leftStickAttack();
      rightStickAttack();
      leftStickAttack();
      rightStickAttack();
      leftStickAttack();
      rightStickAttack();
      leftStickAttack();
      rightStickAttack();
      leftStickAttack();
      delay(750);
      bothStickAttack();
      delay(750);
      bothStickAttack();
      delay(750);
      bothStickAttack();
      delay(750);
      bothStickAttack();
      delay(750);
      bothStickAttack();
      delay(750);
      bothStickAttack();
      delay(1000);
      leftStickAttack();
      rightStickAttack();
      leftStickAttack();
      rightStickAttack();
      leftStickAttack();
      rightStickAttack();
      leftStickAttack();
      rightStickAttack();
      leftStickAttack();
      leftStickAttack();
      leftStickAttack();
      leftStickAttack();
      leftStickAttack();
      leftStickAttack();
      leftStickAttack();
      leftStickAttack();
      rightStickAttack();
      rightStickAttack();
      rightStickAttack();
      rightStickAttack();
      rightStickAttack();
      rightStickAttack();
      
    }
  }



  
}


void leftStickAttack() {
  // Left Down
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  delay(100);
  // Left Up
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  delay(75);
}

void rightStickAttack() {
  //Right Down
  digitalWrite(IN4, HIGH);
  digitalWrite(IN3, LOW);
  delay(100);
  //Right Up
  digitalWrite(IN4, LOW);
  digitalWrite(IN3, HIGH);
  delay(75);
}

void bothStickAttack(){
  //Down
  digitalWrite(IN1, HIGH);
  digitalWrite(IN2, LOW);
  digitalWrite(IN4, HIGH);
  digitalWrite(IN3, LOW);
  delay(100);
  //Up
  digitalWrite(IN1, LOW);
  digitalWrite(IN2, HIGH);
  digitalWrite(IN4, LOW);
  digitalWrite(IN3, HIGH);
  delay(75);
}





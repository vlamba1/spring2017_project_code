#include<avr/io.h>
#include<avr/wdt.h>
#define CLOCK_OUT 11 // OC2A is pin 11

void setup() {
  cli();
  
  wdt_reset();  //Kick watchdog
  WDTCSR |= (1<<WDE | 1 << WDCE); //Watchdog change enable
  WDTCSR = ( 1<< WDE | 1 << WDP3);  //Set watchdog  125 ms system reset mode

  sei();

  setUpPacer();
  
  TCCR1A = 0;
  TCCR1B = 0;
  TCCR1B = (1 << CS12) + (1 << CS11) + (1 << CS10); // Set clk source pin 5
  
  pinMode(CLOCK_OUT, OUTPUT);
  digitalWrite(CLOCK_OUT, LOW);
  Serial.begin(9600);
}

void loop() {
  TCNT1 = 0;
  delay(100);
  Serial.println((TCNT1)*10);
  wdt_reset();
}

void setUpPacer() {
  
  // Set up 15.625 kHz signal on pin 11
  TCCR2A = 0;
  TCCR2B = 0;
  
  OCR2A = 3;
  DDRB |= (1<<PB3); // OC2A is an output
  TCNT2   = 0x00; // Reset the counter
  TCCR2A |= (1<<WGM21); // Set waveform generation type to CTC (type 2)
  TCCR2A |= (1<<COM2A0); // Output compare set to toggle

  TCNT2 = 0x00; // Reset the counter
  TCCR2B |= (1<<CS22) | (1<<CS20); // Set pre-scaler to 128 (enable timer)
}


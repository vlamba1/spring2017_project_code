#include<avr/io.h>
#include<avr/wdt.h>
#define IN 7
#define OUT 8

void setup() {
  cli();
  
  wdt_reset();  //Kick watchdog
  WDTCSR |= (1<<WDE | 1 << WDCE); //Watchdog change enable
  WDTCSR = ( 1<< WDE | 1 << WDP3);  //Set watchdog  125 ms system reset mode

  sei();
  
  pinMode(OUT, OUTPUT);
  digitalWrite(OUT, LOW);
  pinMode(IN, INPUT);
  
}

void loop() {
  if(digitalRead(IN)) {
    digitalWrite(OUT, HIGH);
    delay(100);
    digitalWrite(OUT, LOW);
  }
  wdt_reset();
}

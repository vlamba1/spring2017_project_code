#include<avr/io.h>
#include<avr/wdt.h>
#define OUT 7

void setup() {
  cli();
  
  wdt_reset();  //Kick watchdog
  WDTCSR |= (1<<WDE | 1 << WDCE); //Watchdog change enable
  WDTCSR = ( 1<< WDE | 1 << WDP3);  //Set watchdog  125 ms system reset mode

  sei();
  
  pinMode(OUT, OUTPUT);
  Serial.begin(9600);
  
}

void loop() {

  if(Serial.available() > 0) {
    Serial.read() == 'a' ? digitalWrite(OUT, HIGH) : digitalWrite(OUT, LOW);
    delay(10);
    digitalWrite(OUT,LOW);
  }
  
  wdt_reset();
}

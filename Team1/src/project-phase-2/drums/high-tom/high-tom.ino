#include<avr/io.h>
#include<avr/interrupt.h>
#include<avr/wdt.h>

#define PACER_IN 5
#define IN1 3
#define IN2 4
#define IN3 6
#define IN4 7
#define LONGEST_STROKE_PERIOD 120
#define THIS_STROKE_PERIOD 100 //This is the period of a single up or down stroke

int bpm = 105;
bool isPlaying = false;
bool right = true;
int i;
// 0-A rw|tqtqtetetq|teteteteretetq|tqtqtetetq|teteteretetstste|
// A-B tststetststetststetstste|tststetststetststetstste|tststetststetststetstste|tststetststetststetstste|
// B-C rqreterqtq|teterhtq|rqreterqtq|teterhtq|
// C-D rqreterqtq|teterhtq|rqreterqtq|teterhtq|
// D tststststqrh|tststerqtq
char song[] = "rw|tqtqtetetq|teteteteretetq|tqtqtetetq|teteteretetstste|tststetststetststetstste|tststetststetststetstste|tststetststetststetstste|tststetststetststetstste|rqreterqtq|teterhtq|rqreterqtq|teterhtq|rqreterqtq|teterhtq|rqreterqtq|teterhtq|tststststqrh|tststerqtq";

void setup() {
  cli();
  
  wdt_reset();  //Kick watchdog
  WDTCSR |= (1<<WDE | 1 << WDCE); //Watchdog change enable
  WDTCSR = ( 1<< WDE | 1 << WDP3);  //Set watchdog  125 ms system reset mode

  //Timer 1 set up
  
  TCCR1A = 0; // Set TCCR1A to 0
  TCCR1B = 0; // set TCCR1B to 0
  TCNT1 = 0; // reset t1
  OCR1B = (16000000 / 1024) * (THIS_STROKE_PERIOD * .001) - 1; // set interupts at STROKE_PERIOD ms

  sei();

  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
}

void loop() {
  if(!isPlaying) {
      i = 0;
      playNote(noteLength(song[i++], bpm), song[i++] == 'r');
  }
  wdt_reset();
}

ISR(TIMER1_COMPA_vect) { // On this interrupt rotuine, the note has finished playing
  TIMSK1 &= ~(1 << OCIE1A); // Turn off compare match A interrupts
  if(i < strlen(song) - 1) {
 char type = song[i++];
    char length;
    //This is a measure indicator and does not have any meaning to the drums
    if (type == '|') {
      type = song[i++];
      length = song[i++];
    } else {
      length = song[i++];
    }
    playNote(noteLength(length, bpm), type == 'r'); //play next
}  else {
    isPlaying = false;
    TCCR1A = 0; // Set TCCR1A to 0
    TCCR1B = 0; // set TCCR1B to 0
    TCNT1 = 0; // reset t1
    delay(1000);
  }
}

ISR(TIMER1_COMPB_vect) { // On this interrupt routine, the downstroke is complete
  TIMSK1 &= ~(1 << OCIE1B); // Disables compare match B interrupts
  upStroke(); // Begins the upstroke
}

/*
 * Begins moving the stick down
 */
void downStroke() {
  if(right) {
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, HIGH);
  } else {
    digitalWrite(IN3, LOW);
    digitalWrite(IN4, HIGH);
  }
}

/*
 * Begins moving the stick up
 */
void upStroke() {
  if (right){
    digitalWrite(IN1, HIGH);
    digitalWrite(IN2, LOW);
  } else {
    digitalWrite(IN3, HIGH);
    digitalWrite(IN4, LOW);
  }
  right = !right;
}

/*
 * Sets up the timers
 */
void playNote(int noteLength, bool rest){
  isPlaying = true;
  TCNT1 = 0;
  OCR1A = (16000000 / 1024) * (noteLength * .001) - 1;

  delay(LONGEST_STROKE_PERIOD - THIS_STROKE_PERIOD);
  
  TCCR1A = 0;
  TCCR1B = 0;
  TIMSK1|= (1 << OCIE1A) | (1 << OCIE1B);
  TCCR1B = (1 << CS12) + (1 << CS11) + (1 << CS10); // Set clk source pin 5
  if(!rest) {
    downStroke();
  }
}

/*
 * Calculates the length of a note in ms
 * 
 * Input: note - string representing the note
 *        bpm - int reperesenting the beats per minute of the song
 *        
 * Output: length in ms of the note to be played
 */
int noteLength(char note, int bpm) {
  double beatLength = 60000 / (double) bpm;
  switch(note) {
    case 'w':
      Serial.println(beatLength / .25);
      return beatLength / .25;
      break;
    case 'h':
      Serial.println(beatLength / .5);
      return beatLength / .5;
      break;
    case 'd':
      return (2 * beatLength) / 3;
      break;
    case 'q':
      return beatLength;
      break;
    case 'e':
      return beatLength / 2;
      break;
    case 't':
      return beatLength / 3;
      break;
    case 's':
      return beatLength / 4;
      break;
    case 'x':
      return beatLength / 6;
      break;
  }
}


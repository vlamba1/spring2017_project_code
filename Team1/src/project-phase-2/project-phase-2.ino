#include<avr/io.h>
#include<avr/interrupt.h>
#include<avr/wdt.h>

#define IN1 2
#define IN2 3
#define IN3 4
#define IN4 5
#define STROKE_PERIOD 60 //This is the period of a single up or down stroke

bool right = true; //toggles which stick is played
int bpm = 115;
bool isPlaying = false;
int i;
char* currentSong;
//Gallup: ththtqtqtetetetetststetststetststetststetetststetststetststetststststetststetetststetststststetststetetststetststststetetststststetetststststetetststststetetstststststststststststststststststststststststststststststststststs
//Count off: ththtqtqtetetete
//Note demonstration: tqtqtdtdtdtqtqtetetetetqtqtttttttttttttqtqtststststststststqtqtxtxtxtxtxtxtxtxtxtxtxtx
//halt: tststststqtststststqtqterststw
char song1[] = "tqrqtqretststwtqrqtqretststwththtqtqtetetetetststetststetststetststetetststetststetststetststststetststetetststetststststetststetetststetststststetetststststetetststststetetststststetetststststststststststststststststststststststststststststststststststqrqtqretststwtqrqtqretststwtststststqtststststqtqtetrtstwtststststqtststststqtqterststw";
char song2[] = "tststststststststststststststststststststststststststststststststststststststststststststststststststststststststststststststststqrqterstsreterhterstsreterhterstsreterhtqrqterstsreterhterstsreterhterstsreterhtqtqtqtqtetetetetetetetetqtqtqtqtetetetetetetetetqtqtqtqtetetetetetetetetqtqtqtqtqtqterststqtqtqtqtqtetetetetetetete";
char song3[] = "tststststqrhtststststqrqretetststststqtststststetetststststqtststststetetststststetetststststetetststststetetststststetetststststststststststststststststststststststststxtxtxtxtxtxtxtxtxtxtxtxrwrwtststststqrhtststststqrqretetststststqtststststetetststststqtststststetetststststetetststststetetststststetetststststetetststststststststststststststststststststststststxtxtxtxtxtxtxtxtxtxtxtx";

void setup() {
  cli();
  
  wdt_reset();  //Kick watchdog
  WDTCSR |= (1<<WDE | 1 << WDCE); //Watchdog change enable
  WDTCSR = ( 1<< WDE | 1 << WDP3);  //Set watchdog  125 ms system reset mode

  //Timer 1 set up
  
  TCCR1A = 0; // Set TCCR1A to 0
  TCCR1B = 0; // set TCCR1B to 0
  TCNT1 = 0; // reset t1
  OCR1B = (16000000 / 1024) * (STROKE_PERIOD * .001) - 1; // set interupts at STROKE_PERIOD ms

  sei();

  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  Serial.begin(9600);
  
}

void loop() {
  if(!isPlaying) {
    if  (Serial.available()>0){
      switch(Serial.read()) {
        case 'a':
          bpm = 115;
          currentSong = song1;
          break;

        case 'b':
          bpm = 109;
          currentSong = song2;
          break;

        case 'c':
          bpm = 100;
          currentSong = song3;
          break;
      }
      i = 0;
      playNote(noteLength(currentSong[i++], bpm), currentSong[i++] == 'r');
    }
  }
  wdt_reset();
}

ISR(TIMER1_COMPA_vect) { // On this interrupt rotuine, the note has finished playing
  TIMSK1 &= ~(1 << OCIE1A); // Turn off compare match A interrupts
  if (i < strlen(currentSong) - 1) { //still has notes
    playNote(noteLength(currentSong[i++], bpm), currentSong[i++] == 'r'); //play next
  }else {
    isPlaying = false;
    TCCR1A = 0; // Set TCCR1A to 0
    TCCR1B = 0; // set TCCR1B to 0
    TCNT1 = 0; // reset t1
  }
}

ISR(TIMER1_COMPB_vect) { // On this interrupt routine, the downstroke is complete
  TIMSK1 &= ~(1 << OCIE1B); // Disables compare match B interrupts
  upStroke(); // Begins the upstroke
}

/*
 * Begins moving the stick down
 */
void downStroke() {
  if(right) {
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, HIGH);
  } else {
    digitalWrite(IN3, LOW);
    digitalWrite(IN4, HIGH);
  }
}

/*
 * Begins moving the stick up
 */
void upStroke() {
  if (right){
    digitalWrite(IN1, HIGH);
    digitalWrite(IN2, LOW);
  } else {
    digitalWrite(IN3, HIGH);
    digitalWrite(IN4, LOW);
  }
  right = !right;
}

/*
 * Sets up the timers
 */
void playNote(int noteLength, bool rest){
  isPlaying = true;
  TCNT1 = 0;
  OCR1A = (16000000 / 1024) * (noteLength * .001) - 1;
  TIMSK1|= (1 << OCIE1A) | (1 << OCIE1B);
  TCCR1B = (1 << WGM12) | (1 << CS12) | (1 << CS10);
  if(!rest) {
    downStroke();
  }
}

/*
 * Calculates the length of a note in ms
 * 
 * Input: note - string representing the note
 *        bpm - int reperesenting the beats per minute of the song
 *        
 * Output: length in ms of the note to be played
 */
int noteLength(char note, int bpm) {
  double beatLength = 60000 / (double) bpm;
  switch(note) {
    case 'w':
      Serial.println(beatLength / .25);
      return beatLength / .25;
      break;
    case 'h':
      Serial.println(beatLength / .5);
      return beatLength / .5;
      break;
    case 'd':
      return (2 * beatLength) / 3;
      break;
    case 'q':
      return beatLength;
      break;
    case 'e':
      return beatLength / 2;
      break;
    case 't':
      return beatLength / 3;
      break;
    case 's':
      return beatLength / 4;
      break;
    case 'x':
      return beatLength / 6;
      break;
  }
}

#include <avr/io.h>
int l1 = 2;
int l2 = 3;
int r1 = 4;
int r2 = 5;
char key = 'z'; //default keystroke
int count = 0;
//xxccvvbbnnm
//ttttttttyyiioouuppuuooiiyyt
//ffggooddqqggiiw

void setup() 
{
  Serial.begin(9600);  
  //left and right sticks
  pinMode(l1, OUTPUT); 
  pinMode(l2, OUTPUT);
  pinMode(r1, OUTPUT);
  pinMode(r2, OUTPUT);

}

void loop() {
    if (Serial.available() > 0) 
    {
      //read the incoming byte
      key = Serial.read();
    }
    if (key=='a') //first pattern
    {
      Serial.println("a");
      while(count<1)
      {
        digitalWrite(l1,LOW);
        digitalWrite(l2,HIGH);
        delay(50);
        digitalWrite(l1,HIGH);
        digitalWrite(l2,LOW);
        delay(10); 
        
        digitalWrite(r1,LOW);
        digitalWrite(r2,HIGH);
        delay(50);
        digitalWrite(r1,HIGH);
        digitalWrite(r2,LOW);
        delay(10);
        count++;
      }
    } 
    
    else if (key == 'd') //third pattern
    {
      while(count<1)
      {
        digitalWrite(l1,LOW);
        digitalWrite(l2,HIGH);
        delay(50);
        digitalWrite(l1,HIGH);
        digitalWrite(l2,LOW);
        delay(400); 
      
        digitalWrite(r1,LOW);
        digitalWrite(r2,HIGH);
        delay(50);
        digitalWrite(r1,HIGH);
        digitalWrite(r2,LOW);
        delay(400);
        
        digitalWrite(l1,LOW);
        digitalWrite(l2,HIGH);
        delay(50);
        digitalWrite(l1,HIGH);
        digitalWrite(l2,LOW);
        delay(200);
        
        digitalWrite(l1,LOW);
        digitalWrite(l2,HIGH);
        delay(50);
        digitalWrite(l1,HIGH);
        digitalWrite(l2,LOW);
        delay(200);
         
        digitalWrite(r1,LOW);
        digitalWrite(r2,HIGH);
        delay(50);
        digitalWrite(r1,HIGH);
        digitalWrite(r2,LOW);
        delay(400);
        
        digitalWrite(l1,LOW);
        digitalWrite(l2,HIGH);
        delay(50);
        digitalWrite(l1,HIGH);
        digitalWrite(l2,LOW);
        delay(200);
        
        digitalWrite(l1,LOW);
        digitalWrite(l2,HIGH);
        delay(50);
        digitalWrite(l1,HIGH);
        digitalWrite(l2,LOW);
        delay(150);

        digitalWrite(r1,LOW);
        digitalWrite(r2,HIGH);
        delay(50);
        digitalWrite(r1,HIGH);
        digitalWrite(r2,LOW);
        delay(200);

        digitalWrite(l1,LOW);
        digitalWrite(l2,HIGH);
        delay(50);
        digitalWrite(l1,HIGH);
        digitalWrite(l2,LOW);
        delay(300);
        
        digitalWrite(l1,LOW);
        digitalWrite(l2,HIGH);
        delay(50);
        digitalWrite(l1,HIGH);
        digitalWrite(l2,LOW);
        delay(150);

        digitalWrite(r1,LOW);
        digitalWrite(r2,HIGH);
        delay(50);
        digitalWrite(r1,HIGH);
        digitalWrite(r2,LOW);
        delay(400);
        count++;
      }
    }
      
    else if (key == 'q')//fifth pattern
    {
       Serial.println("q");
      while (count < 1){
        digitalWrite(l1,LOW);
        digitalWrite(l2,HIGH);
        delay(50);
        digitalWrite(l1,HIGH);
        digitalWrite(l2,LOW);
        delay(400);

        digitalWrite(l1,LOW);
        digitalWrite(l2,HIGH);
        delay(50);
        digitalWrite(l1,HIGH);
        digitalWrite(l2,LOW);
        delay(400);

        digitalWrite(l1,LOW);
        digitalWrite(l2,HIGH);
        delay(50);
        digitalWrite(l1,HIGH);
        digitalWrite(l2,LOW);
        delay(400);

        digitalWrite(l1,LOW);
        digitalWrite(l2,HIGH);
        delay(50);
        digitalWrite(l1,HIGH);
        digitalWrite(l2,LOW);
        delay(200);

        digitalWrite(l1,LOW);
        digitalWrite(l2,HIGH);
        delay(50);
        digitalWrite(l1,HIGH);
        digitalWrite(l2,LOW);
        delay(200);
        count++;
      }
    }
    
    //resets drumsticks
    else if (key == 'r'){
       Serial.println("r");
      while (count < 1){
       digitalWrite(l1,HIGH);
        digitalWrite(l2,LOW);
        digitalWrite(r1,HIGH);
        digitalWrite(r2,LOW);
        delay(100);
        count++;
      }
    }

      else if (key == 't'){
      while (count < 1){
        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(150);
        count++;
      }
     }
     else if (key == 'y'){
      while (count < 1){
        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(300);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(150);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(150);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(300);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(130);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(130);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(200);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(150);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(300);
        count++;
      }
     }

     else if (key == 'u'){
      while (count < 1){
        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(150);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(150);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(150);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(150);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(100);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(150);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(150);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(100);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(150);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(150);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(100);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(150);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(150);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(150);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(150);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(150);
        count++;
      }
     }

     else if (key == 'i'){
      while (count < 1){
        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(550);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(150);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(300);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(130);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(130);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(200);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(150);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(300);
        count++;
      }
     }

     else if (key == 'o'){
      while (count < 1){
        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(100);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(700);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(150);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(400);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(70);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(250);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(100);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(700);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(150);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(200);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(250);
        count++;
      }
     }

     else if (key == 'p'){
      while (count < 1){
        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(30);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(200);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(30);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(200);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(30);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(200);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(30);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(200);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(150);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(200);
        count++;
      }
     }

    else if (key == 'f'){
      while (count < 1){
        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(400);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(400);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(400);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(150);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(200);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(400);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(400);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(150);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(150);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(150);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(200);
        count++;
      }
    }

    else if (key == 'g'){
      while (count < 1){
        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(150);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(150);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(200);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(150);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(200);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(150);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(200);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(150);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(400);
        count++;
      }
    }

    else if (key == 'h'){
      while (count < 1){
        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(70);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(700);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(200);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(400);
        count++;
      }
    }

    else if (key == 'x'){
      while (count < 1){
        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(100);
        
        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(100);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(900);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(100);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(450);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(100);
        
        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(100);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(900);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(250);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(300);
        count++;
      }
    }

    else if (key == 'c'){
      while (count < 1){
        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(100);
        
        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(100);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(900);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(100);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(500);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(100);
        
        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(100);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(500);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(100);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(250);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(300);
        count++;
      }
    }

    else if (key == 'v'){
      while (count < 1){
        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(500);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(500);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(500);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(250);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(200);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(250);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(250);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(500);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(250);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(250);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(250);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);
        count++;
      }
    }

    else if (key == 'b'){
      while (count < 1){
        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(100);
        
        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(100);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(500);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);
        
        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(100);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(1000);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(500);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);
        
        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(100);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(250);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(250);
        count++;
      }
    }

    else if (key == 'n'){
      while (count < 1){
        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(100);
        
        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(100);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(250);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(100);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(250);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(100);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(250);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(250);
        count++;
      }
    }

    else if (key == 'm'){
      while (count < 1){
        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(100);
        
        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(100);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(250);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(100);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(250);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(70);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(200);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(50);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(150);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(50);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(100);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(50);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(100);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(50);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(70);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(50);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(100);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(100);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(200);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(150);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(250);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(200);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(300);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(300);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(500);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(600);

        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(800);

        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        delay(700);
        count++;
      }
    }

    else if (key == 'w'){
      while (count < 1){
        digitalWrite(l1, LOW);
        digitalWrite(l2, HIGH);
        digitalWrite(r1, LOW);
        digitalWrite(r2, HIGH);
        delay(50);
        digitalWrite(l1, HIGH);
        digitalWrite(l2, LOW);
        digitalWrite(r1, HIGH);
        digitalWrite(r2, LOW);
        delay(700);
        count++;
      }
    }
    
    count = 0; //resets count 
    key = 'z'; //resets keystroke
    
}

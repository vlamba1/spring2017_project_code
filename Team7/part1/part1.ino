//configued OneSheeld (Bluetooth Sheild)
//go to this website and download the library to make 1sheeld works 
/**************************************************
 * https://1sheeld.com/tutorials/getting-started/ *
 **************************************************/
#define CUSTOM_SETTINGS
#define INCLUDE_VOICE_RECOGNIZER_SHIELD
#include <OneSheeld.h>

#include<avr/wdt.h>
int patternsOne[10][20] = {  
                          {1,0,1,0,0,1,0,0,0,1,0,0,0,1,1,1,0,0,0,1},
                          {0,1,0,1,1,0,1,0,0,1,1,0,1,0,1,0,0,1,1,0}, 
                          {1,1,1,1,0,1,1,1,1,0,1,1,1,1,0,1,1,1,1,0},
                          {1,1,1,0,0,1,1,1,0,0,1,1,1,0,0,1,1,1,0,0},
                          {1,0,1,0,1,1,1,0,1,0,1,0,1,1,1,0,1,0,1,1},
                          {1,0,1,0,0,1,0,0,0,1,0,0,0,1,1,1,0,0,0,1},
                          {0,1,0,1,1,0,1,0,0,1,1,0,1,0,1,0,0,1,1,0}, 
                          {1,1,1,1,0,1,1,1,1,0,1,1,1,1,0,1,1,1,1,0},
                          {1,1,1,0,0,1,1,1,0,0,1,1,1,0,0,1,1,1,0,0},
                          {1,0,1,0,1,1,1,0,1,0,1,0,1,1,1,0,1,0,1,1}
                         };
int patternsTwo[10][20] = {  
                          {0,1,0,1,1,0,1,0,0,1,1,0,1,0,1,0,0,1,1,0},
                          {1,0,1,0,0,1,0,0,0,1,0,0,0,1,1,1,0,0,0,1}, 
                          {1,1,1,0,0,1,1,1,0,0,1,1,1,0,0,1,1,1,0,0},
                          {1,1,1,1,0,1,1,1,1,0,1,1,1,1,0,1,1,1,1,0},
                          {1,0,1,0,1,1,1,0,1,0,1,0,1,1,1,0,1,0,1,1},
                          {0,1,0,1,1,0,1,0,0,1,1,0,1,0,1,0,0,1,1,0},
                          {1,0,1,0,0,1,0,0,0,1,0,0,0,1,1,1,0,0,0,1}, 
                          {1,1,1,0,0,1,1,1,0,0,1,1,1,0,0,1,1,1,0,0},
                          {1,1,1,1,0,1,1,1,1,0,1,1,1,1,0,1,1,1,1,0},
                          {1,0,1,0,1,1,1,0,1,0,1,0,1,1,1,0,1,0,1,1}
                         };

int SongNumberONE[10] = {0,1,2,3,4,5,6,7,8,9};

int SongNumberTWO[10] = {9,9,1,9,9,2,9,9,2,9};

int SongNumberTHREE[10] = {3,4,8,2,7,4,3,1,9,0};

int SongLibrary[4][16] = {{80,0,1,2,3,4,5,6,7,8,9,5,3,1,4,6}, //43s
                          {95,9,2,1,9,3,4,9,5,0,9,6,0,9,7,8}, //48s
                          {100,1,2,2,3,7,7,1,4,1,9,9,1,3,4,0}, //53s
                          {0}};



//variable holds voice input
const char playSongOne[]   = "play song one";
const char playSongTwo[]   = "play song two";
const char playSongThree[] = "play song three";
char incomingByte = '0';
int IN1 = 8;
int IN2 = 7;
int IN3 = 11;
int IN4 = 10;

void watchDogSetup(){
  // disable all interrupts
  cli();
  // reset the WDT timer
  wdt_reset();
  // Enter Watchdog Configuration mode:
  WDTCSR |= (1<<WDCE) | (1<<WDE);
  // Set Watchdog settings: interrupte enable, 0110 for timer (one second)
  WDTCSR = (1<<WDIE) | (1 << WDP0) | (1<<WDP2)| (1<<WDP1) | (1<<WDE);
  //reinable the interrupts
  sei();
  //finish watchdogSetup
}

void setup() {
  // put your setup code here, to run once:
  watchDogSetup();  
  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);
  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  Serial.begin(9600);
  Serial.println("began keyboard");
  OneSheeld.begin();
  VoiceRecognition.start();
}

void playSong(int songNumber)
{
  if(songNumber >= 4){
    Serial.println("Song doesn't exist");
  }else if(SongLibrary[songNumber][0] <=35){
    Serial.println("Delay not long enough");
  }else{
    //code to play any song
    int i=0, j=0;
    int k = songNumber;
    int temp;
    for(i=1; i<16; i++)
    {
      Serial.println(i);
      for(j=0; j<20; j++)
      {
        if(patternsOne[SongLibrary[k][i]][j] == 1 && patternsTwo[SongLibrary[k][i]][j] == 1 ) //Both Sticks
        {
          bothSticks(SongLibrary[k][0]); //Left stick plays 75 ms
        }
        if(patternsOne[SongLibrary[k][i]][j] == 1 && patternsTwo[SongLibrary[k][i]][j] == 0) //Left Stick
        {
          leftStick(SongLibrary[k][0]); //Left stick plays 75 ms
        }
        if(patternsOne[SongLibrary[k][i]][j] == 0 && patternsTwo[SongLibrary[k][i]][j] == 1) //Right Stick
        {
          rightStick(SongLibrary[k][0]); //Left stick plays 75 ms
        }
        if(patternsOne[SongLibrary[k][i]][j] == 0 && patternsTwo[SongLibrary[k][i]][j] == 0){ //Changed to check if there was a zero
          delay(SongLibrary[k][0]); //Changed from 200 to 85
        }
          //delay(200);
          wdt_reset();// incase of long delays
        
      }
    }
  }
}
  
void attack(int t){    
    int i = 0;
  while(i<2){
    wdt_reset();
    delay(t);
    i++;
    }
}

void loop() 
{
  wdt_reset(); 
  int bpm = 85;
  //check if there is keyboard input
  /*
  if(Serial.available() > 0)
  {
    incomingByte = Serial.read();
    
      if(incomingByte == 'a'){        
        leftStick(bpm);
      }else if(incomingByte == 'b'){        
        rightStick(bpm);        
      }else if (incomingByte == 'c'){
        bothSticks(bpm);
      }else if (incomingByte == 'd'){
        delay(bpm);
        Serial.println("Delay");  
        }
      if(incomingByte == '0')
      {
        Serial.println("zero registered");
        int songNumber = 0;
        playSong(songNumber);
      }else if(incomingByte == '1'){
        Serial.println("one registered");
        int songNumber = 1;
        playSong(songNumber);
      }else if(incomingByte == '2'){
        Serial.println("two registered");
        int songNumber = 2;
        playSong(songNumber);
      }else if(incomingByte == '3'){
        Serial.println("three registered");
        int songNumber = 3;
        playSong(songNumber);
      }
    }*/
  if(VoiceRecognition.isNewCommandReceived())
      {
        if(!strcmp(playSongOne,VoiceRecognition.getLastCommand()))
      {
           int songNumber = 0;
           playSong(songNumber);
       }
        if(!strcmp(playSongTwo,VoiceRecognition.getLastCommand()))
        {
          int songNumber = 1;
          playSong(songNumber);
        }
         if(!strcmp(playSongThree,VoiceRecognition.getLastCommand()))
        {
           int songNumber = 2;
           playSong(songNumber);
        }
    }
}

void leftStick(int bpm){

      Serial.print("Left Stick, ");
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, LOW);
      
      attack(35);
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, HIGH);
      attack(bpm-35);
}

void rightStick(int bpm)
{
      Serial.println("Right Stick");
     
      digitalWrite(IN3, HIGH);
      digitalWrite(IN4, LOW);
      
      attack(35);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, HIGH);
      attack(bpm-35);
}
void bothSticks(int bpm)
{//Created to play both sticks at the same time
      Serial.println("Both Sticks");

      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, LOW);
      digitalWrite(IN3, HIGH);
      digitalWrite(IN4, LOW);
      
      attack(35);
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, HIGH);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, HIGH);
      attack(bpm-35);
}

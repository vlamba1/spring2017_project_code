#include <Wire.h>

void setup() {
  Wire.begin();
  Serial.begin(9600);
}

byte x = 1;

void loop() {
  char input = Serial.read();
  if(input == '1') {
    Serial.println("Starting");
    Wire.beginTransmission(8); // transmit to device #8 (all devices)
    Wire.write(x);              // sends one byte
    Wire.endTransmission();    // stop transmitting
    delay(100);
  }
}

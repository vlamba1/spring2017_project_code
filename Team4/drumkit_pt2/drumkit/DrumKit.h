#ifndef DrumKit_h
#define DrumKit_h

#include <avr/wdt.h>
#include "Arduino.h"

class DrumKit
{
	public:
		DrumKit(int pin1, int pin2, int pin3, int pin4);
    void startUp();
    double beat;
    double bpm;
		
		void wn1();
		void hn1();
		void qn1();
		void en1();
		void sn1();
		void tn1();
		
		void wn2();
		void hn2();
		void qn2();
		void en2();
		void sn2();
		void tn2();
		
		void wnA();
		void hnA();
		void qnA();
		void enA();
		void snA();
		void tnA();
		
		void wr();
		void hr();
		void qr();
		void er();
		void sr();
		void tr();
		
		void wTri1();
		void hTri1();
		void qTri1();
		void eTri1();
		
		void wTri2();
		void hTri2();
		void qTri2();
		void eTri2();
		
		void dwn1();
		void dhn1();
		void dqn1();
		void den1();
		void dsn1();
		void dtn1();
		
		void dwn2();
		void dhn2();
		void dqn2();
		void den2();
		void dsn2();
		void dtn2();
		
		void dwnA();
		void dhnA();
		void dqnA();
		void denA();
		void dsnA();
		void dtnA();
		
		void dwr();
		void dhr();
		void dqr();
		void der();
		void dsr();
		void dtr();
    
	private:
  
    void push1();
    void pull1();
    void push2();
    void pull2();
    
		void makeStdBeat(double beat, int8_t phase, int8_t denom, bool stick1, bool stick2);
		void makeTriplet(double beat, int8_t phase, int8_t denom, bool stick1First);
		bool overrideStick1;
		bool overrideStick2;
};

#endif

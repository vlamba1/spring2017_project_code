#include "DrumKit.h"
#include <avr/wdt.h>
#include "Arduino.h"
int _pin1;
int _pin2;
int _pin3;
int _pin4;
double beat = 4.0;
/*int8_t bar = 4;*/
double bpm = 60.0;
//Determine if stick1 and 2 need to be reset after a triplet
bool overrideStick1 = false;
bool overrideStick2 = false;

DrumKit::DrumKit(int pin1, int pin2, int pin3, int pin4)
{
  _pin1 = pin1;
  _pin2 = pin2;
  _pin3 = pin3;
  _pin4 = pin4;
}

  
  void DrumKit::startUp() {
  //configure watchdog
  cli();
  wdt_reset();
  WDTCSR |= (1<<WDCE)|(1<<WDE)& ~(1<<WDIE);
 /*set timeout to 4 seconds*/
  WDTCSR = (1<<WDE) | (1<<WDP3);
  sei();

  /*Define actuator pins and functions*/
  /*first stick*/
  pinMode(_pin1, OUTPUT);
  pinMode(_pin2, OUTPUT);
  /*second stick*/
  pinMode(_pin3, OUTPUT);
  pinMode(_pin4, OUTPUT);
  
  /*begin serial console logging and input*/
  //Serial.begin(9600);
  }
  
  void DrumKit::push1(){ digitalWrite(_pin2,HIGH);digitalWrite(_pin1,LOW);}
  void DrumKit::pull1(){ digitalWrite(_pin1,HIGH);digitalWrite(_pin2,LOW);}

  void DrumKit::push2(){ digitalWrite(_pin4,HIGH);digitalWrite(_pin3,LOW);}
  void DrumKit::pull2(){ digitalWrite(_pin3,HIGH);digitalWrite(_pin4,LOW);}

  /*Standard beats*/
  /*stick 1*/
  void DrumKit::wn1(){ makeStdBeat(beat, 30, 1, true, false);}
  void DrumKit::hn1(){ makeStdBeat(beat, 30, 2, true, false);}
  void DrumKit::qn1(){ makeStdBeat(beat, 30, 4, true, false);}
  void DrumKit::en1(){ makeStdBeat(beat, 30, 8, true, false);}
  void DrumKit::sn1(){ makeStdBeat(beat, 30, 16, true, false);}
  void DrumKit::tn1(){ makeStdBeat(beat, 30, 32, true, false);}
  
  /*stick 2*/
  void DrumKit::wn2(){ makeStdBeat(beat, 30, 1, false, true);}
  void DrumKit::hn2(){ makeStdBeat(beat, 30, 2, false, true);}
  void DrumKit::qn2(){ makeStdBeat(beat, 30, 4, false, true);}
  void DrumKit::en2(){ makeStdBeat(beat, 30, 8, false, true);}
  void DrumKit::sn2(){ makeStdBeat(beat, 30, 16, false, true);}
  void DrumKit::tn2(){ makeStdBeat(beat, 30, 32, false, true);}

  /*both sticks*/
  void DrumKit::wnA(){ makeStdBeat(beat, 30, 1, true, true);}
  void DrumKit::hnA(){ makeStdBeat(beat, 30, 2, true, true);}
  void DrumKit::qnA(){ makeStdBeat(beat, 30, 4, true, true);}
  void DrumKit::enA(){ makeStdBeat(beat, 30, 8, true, true);}
  void DrumKit::snA(){ makeStdBeat(beat, 30, 16, true, true);}
  void DrumKit::tnA(){ makeStdBeat(beat, 30, 32, true, true);}
  
  /*define rest lengths*/
  void DrumKit::wr(){ makeStdBeat(beat, 30, 1, false, false);}
  void DrumKit::hr(){ makeStdBeat(beat, 30, 2, false, false);}
  void DrumKit::qr(){ makeStdBeat(beat, 30, 4, false, false);}
  void DrumKit::er(){ makeStdBeat(beat, 30, 8, false, false);}
  void DrumKit::sr(){ makeStdBeat(beat, 30, 16, false, false);}
  void DrumKit::tr(){ makeStdBeat(beat, 30, 32, false, false);}

  /*triplets*/
  void DrumKit::wTri1(){ makeTriplet(beat, 20, 1, true);}
  void DrumKit::hTri1(){ makeTriplet(beat, 20, 2, true);}
  void DrumKit::qTri1(){ makeTriplet(beat, 20, 4, true);}
  void DrumKit::eTri1(){ makeTriplet(beat, 20, 8, true);}
  
  void DrumKit::wTri2(){ makeTriplet(beat, 20, 1, true);}
  void DrumKit::hTri2(){ makeTriplet(beat, 20, 2, true);}
  void DrumKit::qTri2(){ makeTriplet(beat, 20, 4, true);}
  void DrumKit::eTri2(){ makeTriplet(beat, 20, 8, true);}

  /*Dotted Beats*/
  /*stick 1*/
  void DrumKit::dwn1(){ makeStdBeat(beat, 45, 1, true, false);} 
  void DrumKit::dhn1(){ makeStdBeat(beat, 45, 2, true, false);}
  void DrumKit::dqn1(){ makeStdBeat(beat, 45, 4, true, false);}
  void DrumKit::den1(){ makeStdBeat(beat, 45, 8, true, false);}
  void DrumKit::dsn1(){ makeStdBeat(beat, 45, 16, true, false);}
  void DrumKit::dtn1(){ makeStdBeat(beat, 45, 32, true, false);}
  
  /*stick 2*/
  void DrumKit::dwn2(){ makeStdBeat(beat, 45, 1, false, true);}
  void DrumKit::dhn2(){ makeStdBeat(beat, 45, 2, false, true);}
  void DrumKit::dqn2(){ makeStdBeat(beat, 45, 4, false, true);}
  void DrumKit::den2(){ makeStdBeat(beat, 45, 8, false, true);}
  void DrumKit::dsn2(){ makeStdBeat(beat, 45, 16, false, true);}
  void DrumKit::dtn2(){ makeStdBeat(beat, 45, 32, false, true);}
  
  /*both sticks*/
  void DrumKit::dwnA(){ makeStdBeat(beat, 45, 1, true, true);} 
  void DrumKit::dhnA(){ makeStdBeat(beat, 45, 2, true, true);}
  void DrumKit::dqnA(){ makeStdBeat(beat, 45, 4, true, true);}
  void DrumKit::denA(){ makeStdBeat(beat, 45, 8, true, true);}
  void DrumKit::dsnA(){ makeStdBeat(beat, 45, 16, true, true);}
  void DrumKit::dtnA(){ makeStdBeat(beat, 45, 32, true, true);}

    /*define rest lengths*/
  void DrumKit::dwr(){ makeStdBeat(beat, 45, 1, false, false);}
  void DrumKit::dhr(){ DrumKit::makeStdBeat(beat, 45, 2, false, false);}
  void DrumKit::dqr(){ makeStdBeat(beat, 45, 4, false, false);}
  void DrumKit::der(){ makeStdBeat(beat, 45, 8, false, false);}
  void DrumKit::dsr(){ makeStdBeat(beat, 45, 16, false, false);}
  void DrumKit::dtr(){ makeStdBeat(beat, 45, 32, false, false);}


  void DrumKit::makeStdBeat(double beat, int8_t phase, int8_t denom, bool stick1, bool stick2){
    if(overrideStick1){
      DrumKit::pull1();
      stick1=!overrideStick1;
      stick2 = true;
    }
    if(overrideStick2){
      DrumKit::pull2();
      stick2=!overrideStick2;
      stick1 = true;
    }
    if(stick1){
      DrumKit::push1();
    }
    if(stick2){
      DrumKit::push2();
    }
    delay(phase*(beat/denom)*1000/bpm);
    if(stick1){
      DrumKit::pull1();
    }
    if(stick2){
      DrumKit::pull2();
    }
    delay(phase*(beat/denom)*1000/bpm);
    overrideStick1 = false;
    overrideStick2 = false;
    wdt_reset();
  }

  void DrumKit::makeTriplet(double beat, int8_t phase, int8_t denom, bool stick1First){
    stick1First = (stick1First && !overrideStick1) || overrideStick2;//override stick 1 being first if supposed to
    if(stick1First){
      pull2();
      push1();
      overrideStick1=true;
      overrideStick2=false;//make sure next beat played doesn't start with stick1
    } else {
      pull1();
      push2();
      overrideStick1=false;
      overrideStick2=true;
    }
    delay(phase*(beat/denom)*1000/bpm);
    if (stick1First){
      pull1();
      push2();
    } else {
      pull2();
      push1();
    }
    delay(phase*(beat/denom)*1000/bpm);
    if (stick1First){
      pull2();
      push1();
    } else {
      pull1();
      push2();
    }
    delay(phase*(beat/denom)*1000/bpm);
    wdt_reset();
  }

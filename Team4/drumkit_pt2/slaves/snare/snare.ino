#include <Wire.h>
#include <DrumKit.h>

DrumKit d = DrumKit(5,6,9,10);
bool run = false;

void setup() {
  d.startUp();
  Wire.begin(8);
  Wire.onReceive(receiveEvent);
  Serial.begin(9600);
  d.beat = 4.0;
  d.bpm = 150.0;
  #define pattern0 d.hr(); d.hnA();
  #define pattern1 {for(int8_t i=0; i<3; i++) pattern0;}
  #define pattern2 {d.hr(); d.qnA(); d.qnA();}
  #define pattern3 {d.hr(); d.qr(); d.qnA();}
}

void loop() {
  if(run){
    Serial.println("Starting");
    //measures 1-3
    pattern1;
    //measure 4
    pattern2;
    //measures 5-7
    pattern1;
    //measure 8
    pattern2;
    //measure 9
    pattern0;
    //measure 10
    pattern3;
    //measure 11
    pattern0;
    //measure 12
    pattern2;
    //measures 13-19
    for(int8_t j=0; j<7; j++) d.wr();
    //measure 20
    pattern3;
    //measure 21
    d.qr(); d.er(); d.en1(); d.hn2();
    //measure 22
    pattern0;
    run=false;
    Serial.println("Done");
  }
}

void receiveEvent(int myNumber) {
  run = true;
  Serial.println("Done2");
}


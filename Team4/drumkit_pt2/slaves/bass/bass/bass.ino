#include <Wire.h>
#include <DrumKit.h>

DrumKit d = DrumKit(5,6,9,10);
bool run=false;

void setup() {
  d.startUp();
  Wire.begin(8);
  Wire.onReceive(receiveEvent);
  d.beat = 4.0;
  d.bpm = 150.0;
}

void loop() {
  if(run){
    playPattern();
  }
}

void playPattern() {
  for(int8_t i=0; i<2; i++){
     //Measure 1&5
    d.qnA();d.qnA();d.er();d.qnA();d.er();
    //Measure 2&6
    d.er();d.qnA();d.qnA();d.qnA();d.er();
    //Measure 3&7
    d.qnA();d.qnA();d.er();d.qnA();d.er();
    //Measure 4&8
    d.er();d.qnA();d.qnA();d.qnA();d.er(); 
  }

  for(int8_t i=0; i<6; i++){
    //measure 9&11&13&15&17&19
    d.qnA();d.qnA();d.er();d.qnA();d.er();
    //measure 10 & 12&14&16&18&20
    d.er();d.qnA();d.qnA();d.qnA();d.er();
  }

  //measure 21
  d.qnA();d.qnA();d.qr();d.qnA();
  //measure 22
  d.hr();d.er();d.qnA();d.er();
  run=false;
}

void receiveEvent(int myNumber){
  run=true;
}

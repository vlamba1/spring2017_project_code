#include <Wire.h>
#include <DrumKit.h>

DrumKit d = DrumKit(5,6,9,10);
bool run=false;

void setup() {
  d.startUp();
  Wire.begin(8);
  Wire.onReceive(receiveEvent);
  d.beat = 4.0;
  d.bpm =150.0;
}

void loop() {
  if(run){
    playPattern();
  }
}

void playPattern() {
 //measures 1- 8
 for(int8_t j=0; j<2; j++){
  //measure 1, 5
  d.wnA();
  //measure 2-4, 6-8
  for(int8_t i=0; i<3; i++){
    d.wr();
  }
 }
 //measure 9-12
  for(int8_t i=0; i<4; i++){
    d.wr();
  }
  for(int8_t i=0; i<4; i++){
  //measure 13,15,17,19
  d.hnA();d.qr();d.enA();d.enA();
  //measure 14,16,18,20
  d.wr();
  }
  //measure 21
  d.qnA();d.hnA();d.qnA();
  //measure 22
  d.hr();d.qr();d.qnA();
  run=false;
}

void receiveEvent(int myNumber){
  run=true;
}

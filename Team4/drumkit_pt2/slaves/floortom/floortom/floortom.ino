#include <Wire.h>
#include <DrumKit.h>

DrumKit d = DrumKit(5,6,9,10);
bool run = false;

void setup() {
  d.startUp();
  Wire.begin(8);
  Wire.onReceive(receiveEvent);
  d.beat = 4.0;
  d.bpm = 150.0;
}

void loop() {
  if(run){
    playPattern();
  }
}

void playPattern() {
  //measure 1
  d.er();d.enA();d.er();d.enA();d.qr();d.enA();d.enA();
  //measure 2
  d.er();d.en1();d.qnA();d.qr();d.enA();d.enA();
  //measure 3
  d.en1();d.enA();d.en1();d.enA();d.qr();d.enA();d.enA();
  //measure 4
  d.qn1();d.hn1();d.er();d.enA();
  //measure 5
  d.er();d.enA();d.er();d.enA();d.qr();d.enA();d.enA();
  //measure 6
  d.er();d.en1();d.qnA();d.qr();d.enA();d.enA();
  //measure 7
  d.en1();d.en2();d.en1();d.enA();d.qr();d.enA();d.enA();
  //measure 8
  d.qn1();d.qnA();d.qr();d.er();d.enA();
 
  for(int8_t i=0; i<2; i++){
   //measure 9&11
  d.er();d.qnA();d.qnA();d.er();d.enA();d.enA();
  //measure 10&12
  d.qr();d.qnA();d.qr();d.hr();
  }

  //measure 13
  d.er();d.qnA();d.qnA();d.er();d.enA();d.enA();
  //measure 14
  d.qr();d.qnA();d.qr();d.enA();d.enA();
  
  
  for(int8_t i=0; i<2; i++){
   //measure 15&17
  d.er();d.qnA();d.qnA();d.er();d.enA();d.enA();
  //measure 16&18
  d.qr();d.qnA();d.qr();d.er();d.enA();
  }
  //measure 19
  d.er();d.qnA();d.qnA();d.er();d.enA();d.enA();
  //measure 20
  d.qr();d.qnA();d.hr();

  //measure 21
  d.hr();d.qr();d.qnA();
  //measure 22
  d.wr();
  run=false;
}

void receiveEvent(int myNumber){
  run=true;
}


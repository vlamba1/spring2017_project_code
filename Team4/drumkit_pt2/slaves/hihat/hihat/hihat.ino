#include <Wire.h>
#include <DrumKit.h>

DrumKit d = DrumKit(5,6,9,10);
bool run = false;

void setup() {
  d.startUp();
  Wire.begin(8);
  Wire.onReceive(receiveEvent);
  d.beat = 4.0;
  d.bpm = 150.0;
}

void loop() {
  if(run){
    playPattern();
  }
}

void playPattern(){
  for(int8_t i=0; i<2; i++){
  // measure 1, 5
  d.qr();d.hnA();d.qnA();
  //measure 2,6
  d.qnA();d.qnA();d.qr();d.qnA();
  //measure 3,7
  d.qnA();d.qnA();d.qr();d.qnA();
  //measure 4,8
  d.qnA();d.qnA();d.hr();
  }
  //measure 9
  d.en1();d.en1();d.en1();d.en1();d.er();d.en1();d.en1();d.en1();
  //measure 10
  d.en1();d.en1();d.en1();d.en1();d.en1();d.en1();d.qr();
  //measure 11
  d.en1();d.en1();d.en1();d.en1();d.er();d.en1();d.en1();d.en1();
  //measure 12
  d.en1();d.en1();d.en1();d.en1();
  for(int8_t i=0; i<3; i++){
  //measure 13,15,17
  d.qr();d.qnA();d.qnA();d.qr();
  //measure 14,16,18
  d.qnA();d.qnA();d.qnA();d.qnA();
  }
  //measure 19
  d.qr();d.qnA();d.qnA();d.qr();
  //measure 20
  d.qnA();d.qnA();d.hnA();
  //measure 21 & 22;
  d.wr();d.wr();
  run=false;
}

void receiveEvent(int myNumber){
  run=true;
}


#include <Wire.h>
#include <DrumKit.h>

DrumKit d = DrumKit(5,6,9,10);

void setup() {
  d.startUp();
  Wire.begin(8);                // join i2c bus with address #8
  Wire.onReceive(receiveEvent); // register event
  Serial.begin(9600);           // start serial for output
}

void loop() {
  delay(100);
}

// function that executes whenever data is received from master
// this function is registered as an event, see setup()
void receiveEvent(int howMany) {
  char x = Wire.read();    // receive byte as an integer
  Serial.println(x + " recieved. Playing sequence.");         // print the integer
  //PLAY THE FUNCTION HERE?
}

#include <avr/wdt.h>

  double beat = 4.0;
  /*int8_t bar = 4;*/
  double bpm = 60.0;
  //Determine if stick1 and 2 need to be reset after a triplet
  bool overrideStick1 = false;
  bool overrideStick2 = false;
  void setup() {
  //configure watchdog
  cli();
  wdt_reset();
  WDTCSR |= (1<<WDCE)|(1<<WDE)& ~(1<<WDIE);
 /*set timeout to 4 seconds*/
  WDTCSR = (1<<WDE) | (1<<WDP3);
  sei();

  /*Define actuator pins and functions*/
  /*first stick*/
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  /*second stick*/
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
  
  #define push1 digitalWrite(8,HIGH);digitalWrite(7,LOW);
  #define pull1 digitalWrite(7,HIGH);digitalWrite(8,LOW);

  #define push2 digitalWrite(10,HIGH);digitalWrite(9,LOW);
  #define pull2 digitalWrite(9,HIGH);digitalWrite(10,LOW);

  /*Standard beats*/
  /*stick 1*/
  #define wn1 makeStdBeat(beat, 30, 1, true, false);
  #define hn1 makeStdBeat(beat, 30, 2, true, false);
  #define qn1 makeStdBeat(beat, 30, 4, true, false);
  #define en1 makeStdBeat(beat, 30, 8, true, false);
  #define sn1 makeStdBeat(beat, 30, 16, true, false);
  #define tn1 makeStdBeat(beat, 30, 32, true, false);
  
  /*stick 2*/
  #define wn2 makeStdBeat(beat, 30, 1, false, true);
  #define hn2 makeStdBeat(beat, 30, 2, false, true);
  #define qn2 makeStdBeat(beat, 30, 4, false, true);
  #define en2 makeStdBeat(beat, 30, 8, false, true);
  #define sn2 makeStdBeat(beat, 30, 16, false, true);
  #define tn2 makeStdBeat(beat, 30, 32, false, true);

  /*both sticks*/
  #define wnA makeStdBeat(beat, 30, 1, true, true);
  #define hnA makeStdBeat(beat, 30, 2, true, true);
  #define qnA makeStdBeat(beat, 30, 4, true, true);
  #define enA makeStdBeat(beat, 30, 8, true, true);
  #define snA makeStdBeat(beat, 30, 16, true, true);
  #define tnA makeStdBeat(beat, 30, 32, true, true);
  
  /*define rest lengths*/
  #define wr makeStdBeat(beat, 30, 1, false, false);
  #define hr makeStdBeat(beat, 30, 2, false, false);
  #define qr makeStdBeat(beat, 30, 4, false, false);
  #define er makeStdBeat(beat, 30, 8, false, false);
  #define sr makeStdBeat(beat, 30, 16, false, false);
  #define tr makeStdBeat(beat, 30, 32, false, false);

  /*triplets*/
  #define wTri1 makeTriplet(beat, 20, 1, true);
  #define hTri1 makeTriplet(beat, 20, 2, true);
  #define qTri1 makeTriplet(beat, 20, 4, true);
  #define eTri1 makeTriplet(beat, 20, 8, true);
  
  #define wTri2 makeTriplet(beat, 20, 1, true);
  #define hTri2 makeTriplet(beat, 20, 2, true);
  #define qTri2 makeTriplet(beat, 20, 4, true);
  #define eTri2 makeTriplet(beat, 20, 8, true);

  /*Dotted Beats*/
  /*stick 1*/
  #define dwn1 makeStdBeat(beat, 45, 1, true, false); 
  #define dhn1 makeStdBeat(beat, 45, 2, true, false);
  #define dqn1 makeStdBeat(beat, 45, 4, true, false);
  #define den1 makeStdBeat(beat, 45, 8, true, false);
  #define dsn1 makeStdBeat(beat, 45, 16, true, false);
  #define dtn1 makeStdBeat(beat, 45, 32, true, false);
  
  /*stick 2*/
  #define dwn2 makeStdBeat(beat, 45, 1, false, true); 
  #define dhn2 makeStdBeat(beat, 45, 2, false, true);
  #define dqn2 makeStdBeat(beat, 45, 4, false, true);
  #define den2 makeStdBeat(beat, 45, 8, false, true);
  #define dsn2 makeStdBeat(beat, 45, 16, false, true);
  #define dtn2 makeStdBeat(beat, 45, 32, false, true);
  
  /*both sticks*/
  #define dwnA makeStdBeat(beat, 45, 1, true, true); 
  #define dhnA makeStdBeat(beat, 45, 2, true, true);
  #define dqnA makeStdBeat(beat, 45, 4, true, true);
  #define denA makeStdBeat(beat, 45, 8, true, true);
  #define dsnA makeStdBeat(beat, 45, 16, true, true);
  #define dtnA makeStdBeat(beat, 45, 32, true, true);

    /*define rest lengths*/
  #define dwr makeStdBeat(beat, 45, 1, false, false);
  #define dhr makeStdBeat(beat, 45, 2, false, false);
  #define dqr makeStdBeat(beat, 45, 4, false, false);
  #define der makeStdBeat(beat, 45, 8, false, false);
  #define dsr makeStdBeat(beat, 45, 16, false, false);
  #define dtr makeStdBeat(beat, 45, 32, false, false);

  /*begin serial console logging and input*/
  Serial.begin(9600);
  }

  void makeStdBeat(double beat, int8_t phase, int8_t denom, bool stick1, bool stick2){
    if(overrideStick1){
      pull1;
      stick1=!overrideStick1;
      stick2 = true;
    }
    if(overrideStick2){
      pull2;
      stick2=!overrideStick2;
      stick1 = true;
    }
    if(stick1){
      push1;
    }
    if(stick2){
      push2;
    }
    delay(phase*(beat/denom)*1000/bpm);
    if(stick1){
      pull1;
    }
    if(stick2){
      pull2;
    }
    delay(phase*(beat/denom)*1000/bpm);
    overrideStick1 = false;
    overrideStick2 = false;
    wdt_reset();
  }

  void makeTriplet(double beat, int8_t phase, int8_t denom, bool stick1First){
    stick1First = (stick1First && !overrideStick1) || overrideStick2;//override stick 1 being first if supposed to
    if(stick1First){
      pull2;
      push1;
      overrideStick1=true;
      overrideStick2=false;//make sure next beat played doesn't start with stick1
    } else {
      pull1;
      push2;
      overrideStick1=false;
      overrideStick2=true;
    }
    delay(phase*(beat/denom)*1000/bpm);
    if (stick1First){
      pull1;
      push2;
    } else {
      pull2;
      push1;
    }
    delay(phase*(beat/denom)*1000/bpm);
    if (stick1First){
      pull2;
      push1;
    } else {
      pull1;
      push2;
    }
    delay(phase*(beat/denom)*1000/bpm);
    wdt_reset();
  }
  
  void loop() {
    if (Serial.available() > 0) {
      // read incoming serial data:
      char inChar = Serial.read();
      // Type the next ASCII value from what you received:
      Serial.write(inChar);
      int8_t i =0;
      if(inChar == 'a'){
        bpm=103;
        for (i; i<2; i++){
          yyz();
        }
      } else if(inChar == 'b'){
        bpm = 150;
        for (i; i<7; i++){
          myHero();
        }
      } else if(inChar == 'c'){
        bpm = 109;
        for (i; i<6; i++){
          amenBreak();
        }
        
      }
      /* experimental do not use yet
      else if(inChar == 'd'){
        bpm = 103;
        kobaia();
      }
      */
    }
  }

  void myHero(){
    //measure 1
    qnA;enA;en1;enA;en2;enA;en1;
    //measure 2
    en1;en2;enA;en1;enA;en2;enA;en1;
    //measure 3
    enA;en2;enA;en1;enA;en2;enA;en1;
    //measure 4
    en1;en2;enA;en1;enA;en2;qnA;
    }

  void yyz() {
    //measure 1-6: repeat six times
    for(int8_t i=0; i<6; i++){
    enA;sn2;snA;sr;
    enA;snA;sr;
    sn2;enA;
    enA;enA;
    enA;sn2;sn1;
    }
    //measure 7
    enA;sn2;snA;sr;
    enA;snA;sr;
    sn2;enA;
    enA;enA;
    enA;sn1;sn2;
  }

  void amenBreak(){
    //measures 1 and 2
    for(int8_t i=0; i<2; i++){
      enA;enA;
      enA;sn1;sn2;
      sn1;sn2;snA;sn1;enA;sn2;sn1;
    }
    //measure 3
    enA;enA;enA;sn1;sn2;
    sn1;sn2;enA;en1;enA;
    //measure 4
    sn1;sn2;snA;sn1;
    enA;sn1;sn2;
    sn1;sn2;enA;en1;enA;

  }

  void kobaia(){
    en1;sr;
    for(int8_t i=0; i<3; i++){
      eTri2;
      sr;
      eTri1;
      sr;
     }
     eTri2;en1;

    en2;sr;
    for(int8_t i=0; i<5; i++){
      eTri2;
      sr;
     }
     dsn2;dsn1;dsn2;en1;en2;tr;
   }






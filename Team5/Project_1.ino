/*
 * CSCE236 Embedded Systems Spring 2016
 * 
 * Project 1 Code
 */

#include <avr/io.h>
#include <avr/wdt.h>

#define IN1 7
#define IN2 8

#define IN3 9
#define IN4 10

// Notes
#define FULL 1200
#define HALF 600
#define QUAR 300
#define EIGH 150

//String in;

// Keyboard


void setup() {
  Serial.begin(9600);
  Serial.println("Starting up.");  

 

  pinMode(IN1, OUTPUT);
  pinMode(IN2, OUTPUT);

  pinMode(IN3, OUTPUT);
  pinMode(IN4, OUTPUT);
  
  
  Serial.println("Finished setup");
}

void loop() {
   byte ib = 0;
   int ib1;
   int ib2;

  
  if(Serial.available() > 0){
    delay(5);
    ib = Serial.read();
    //in = Serial.readString();
    Serial.println(ib);
    //Serial.println(in);
    
  }


  // Single Hit
  if(ib == 'K' || ib == 'k'){
    digitalWrite(IN1, LOW);
    digitalWrite(IN2, HIGH);
  
    Serial.println("delay1");
   
    delay(100);
  
    digitalWrite(IN1, HIGH);
    digitalWrite(IN2, LOW);
  
    Serial.println("delay2");
    
    delay(1000);
  }


  // Rolling sequence
  if(ib == 'R' || ib == 'r'){
    rollQuar(10);
    rollEigh(10);
    resetStick();
  }

  if(ib == 'T' || ib == 't'){
    routine2(10);
  }

  if(ib == 'Y' || ib == 'y'){
    routine3(10);
  }

  if(ib == 'W' || ib == 'w'){
    routine4(10);
  }

  if(ib == '1'){
    scriptOne();
  }

  if(ib == '2'){
    pattern2();
  }
  if(ib == '3'){
    scriptTwo();
    scriptTwo();
  }
}

void scriptOne(){
  // General Script
  

// gradual Drum roll
  for (int i = 1200; i > 300; i=i-50){
    hitDrumRoll(i);
  }

  for (int i = 300; i > 120; i=i-10){
    hitDrumRoll(i);
  }
  delay(HALF);
  rollQuar(1);
  delay(QUAR);
  rollQuar(1);
  delay(QUAR);
  rollQuar(1);
  delay(QUAR);
  rollQuar(1);
  delay(QUAR);

  routine4(6);

  pattern4();

  hitBoth();
  delay(HALF);
  hitBoth();
  delay(HALF);
  hitBoth();
  delay(HALF);
  
}

void pattern4(){
  hitRight(HALF);
  hitLeft(EIGH);
  hitRight(HALF);
  hitLeft(QUAR);
  rollHalf(6);
  resetStick();
  hitBoth();
  delay(EIGH);
  hitRight(HALF);
  hitLeft(EIGH);
  hitRight(HALF);
  hitLeft(QUAR);
  rollHalf(6);
  resetStick();
  hitBoth();
  delay(EIGH);
  
}

void pattern2() {
  hitLeft(QUAR);
  hitRight(HALF);
  hitLeft(QUAR);
  hitRight(EIGH);
  hitLeft(EIGH);
  hitRight(QUAR);
  hitBoth();

  delay(EIGH);

  rollQuar(6);
  resetStick();

  hitBoth();
  
  delay(EIGH);

  hitBoth();
  delay(HALF);
  hitBoth();
  delay(QUAR);
  hitBoth();
  delay(QUAR);

  rollQuar(2);
  rollEigh(2);
  resetStick();

  hitBoth();
  delay(EIGH);
  hitBoth();
  delay(EIGH);

  hitLeft(HALF);
  hitRight(QUAR);
  hitRight(EIGH);

  hitLeft(QUAR);
  hitLeft(EIGH);
  rollEigh(3);
  resetStick();

  hitBoth();
  delay(HALF);

  rollQuar(6);
  resetStick();

  hitBoth();
  
  delay(EIGH);

  hitBoth();
  delay(HALF);
  hitBoth();
  delay(QUAR);
  hitBoth();
  delay(QUAR);

  hitLeft(HALF);
  hitRight(QUAR);
  hitRight(EIGH);

  hitLeft(QUAR);
  hitLeft(EIGH);
  rollEigh(3);
  resetStick();

  hitLeft(QUAR);
  hitRight(HALF);
  hitLeft(QUAR);
  hitRight(EIGH);
  hitLeft(EIGH);
  hitRight(QUAR);
  hitBoth();

  delay(EIGH);

  rollQuar(6);
  resetStick();

  hitBoth();
  delay(EIGH);
  hitBoth();
  delay(EIGH);

  hitLeft(HALF);
  hitRight(QUAR);
  hitRight(EIGH);

  hitLeft(QUAR);
  hitLeft(EIGH);
  rollEigh(3);
  resetStick();

  hitBoth();
  delay(HALF);

  hitBoth();
  delay(HALF);
  hitBoth();
  delay(QUAR);
  hitBoth();
  delay(QUAR);

  rollQuar(2);
  rollEigh(2);
  resetStick();

  hitLeft(QUAR);
  hitRight(HALF);
  hitLeft(QUAR);
  hitRight(EIGH);
  hitLeft(EIGH);
  hitRight(QUAR);
  hitBoth();

  delay(EIGH);

  rollQuar(6);
  resetStick();

  hitBoth();
  
  delay(EIGH);

  hitBoth();
  delay(HALF);
  hitBoth();
  delay(QUAR);
  hitBoth();
  delay(QUAR);

  hitLeft(HALF);
  hitRight(QUAR);
  hitRight(EIGH);

  hitLeft(QUAR);
  hitLeft(EIGH);
  rollEigh(3);
  resetStick();

  hitBoth();
  delay(HALF);

  hitLeft(HALF);
  hitRight(QUAR);
  hitRight(EIGH);

  hitLeft(QUAR);
  hitLeft(EIGH);
  rollEigh(3);
  resetStick();

  hitLeft(QUAR);
  hitRight(HALF);
  hitLeft(QUAR);
  hitRight(EIGH);
  hitLeft(EIGH);
  hitRight(QUAR);
  hitBoth();

  delay(EIGH);

  rollQuar(2);
  rollEigh(2);
  resetStick();

  hitLeft(QUAR);
  hitRight(HALF);
  hitLeft(QUAR);
  hitRight(EIGH);
  hitLeft(EIGH);
  hitRight(QUAR);
  hitBoth();

  delay(EIGH);

  rollQuar(6);
  resetStick();
}

void scriptTwo() {
  hitRight(QUAR);
  hitLeft(QUAR);

  for(int i = 0; i < 5; i++) {
    hitRight(EIGH);
    hitLeft(EIGH);
  }

  hitRight(QUAR);
  hitLeft(QUAR);

  for(int i = 0; i < 5; i++) {
    hitRight(EIGH);
    hitLeft(EIGH);
  }

  for(int i = 0; i < 4; i++) {
    hitRight(QUAR);
    hitLeft(QUAR);
  }

  hitRight(QUAR);

  delay(250);

  hitLeft(EIGH);
  hitRight(EIGH);

  delay(250);
  
  hitLeft(EIGH);
  hitRight(EIGH);
  hitLeft(EIGH);
  hitRight(EIGH);
  hitLeft(EIGH);

  hitRight(QUAR);
  hitLeft(QUAR);
  hitRight(QUAR);

  hitLeft(EIGH);
  hitRight(EIGH);
  hitLeft(EIGH);
  hitRight(EIGH);
  hitLeft(EIGH);

  delay(250);

  hitLeft(EIGH);
  hitRight(EIGH);
  hitLeft(EIGH);
  hitRight(EIGH);
  hitLeft(EIGH);

  rollQuar(3);
  resetStick();

  for(int i = 0; i < 4; i++) {
    hitRight(QUAR);
    hitLeft(QUAR);
  }  

  hitRight(QUAR);
  
   for(int i = 0; i < 2; i++) {
    hitLeft(QUAR);
    rollQuar(3);
    resetStick();
    hitRight(QUAR);
  }
  
  hitLeft(QUAR);

  rollQuar(4);
  resetStick();
  hitRight(QUAR);
  hitLeft(QUAR);
  hitRight(QUAR);

  hitLeft(EIGH);
  hitRight(EIGH);
  hitLeft(EIGH);
  hitRight(EIGH);
  hitLeft(EIGH);

  rollQuar(6);
  resetStick();
  
  

  
}

void resetStick(){
    digitalWrite(IN1, HIGH);
    digitalWrite(IN2, LOW);

    digitalWrite(IN3, HIGH);
    digitalWrite(IN4, LOW);
}

void rollHalf(int n){

  for (int i = 0; i < n; i++){
      hitDrumRoll(HALF);
      Serial.println("Rolling!");
    }
  
}

void rollQuar(int n){

  for (int i = 0; i < n; i++){
      hitDrumRoll(QUAR);
      Serial.println("Rolling!");
    }
  
}

void rollEigh(int n){

  for (int i = 0; i < n; i++){
      hitDrumRoll(EIGH);
      Serial.println("Rolling!");
    }
  
}

void routine2(int n){
  
  for (int i = 0; i < n; i++){
      
      hitRight(HALF);
      hitLeft(HALF);
      hitRight(QUAR);
      hitRight(QUAR);
      hitLeft(HALF);
      
      Serial.println("Routine 2");
    }
  
}

void routine3( int n){

  for (int i=0; i<n; i++){
    Serial.println("routine 3");

    hitRight(QUAR);
    hitRight(QUAR);
    hitLeft(HALF);

    hitRight(QUAR);
    hitRight(EIGH);
    hitRight(EIGH);
    hitLeft(HALF);
  }
  
}

void routine4(int n){

  for (int i=0; i<n; i++){
    Serial.println("routine 4");

    hitRight(QUAR);
    hitRight(QUAR);
    hitRight(QUAR);
    hitRight(EIGH);
    hitLeft(EIGH);
  }
  
}


void hitLeft(int n){
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, HIGH);
    
     
      delay(100);
    
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, LOW);
    
      
      delay(n-100); 
      Serial.println("Hit!");
}

void hitRight(int n){
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, HIGH);
    
     
      delay(100);
    
      digitalWrite(IN3, HIGH);
      digitalWrite(IN4, LOW);
    
      
      delay(n-100); 
      Serial.println("Hit!");
}

void hitDrumRoll(int n){
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, HIGH);
      digitalWrite(IN3, HIGH);
      digitalWrite(IN4, LOW);
    
     
      delay(n/2);
    
      digitalWrite(IN1, HIGH);
      digitalWrite(IN2, LOW);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, HIGH);
    
      
      delay(n/2);
}

void hitBoth(){
  
      digitalWrite(IN1, LOW);
      digitalWrite(IN2, HIGH);
      digitalWrite(IN3, LOW);
      digitalWrite(IN4, HIGH);

      delay(100);
      
      resetStick();
}


#include <Wire.h>
#define NODE_ADDRESS 5

const int W = 1000;
const int H = 500;
const int Q = 250;
const int E = 125;
const int S = 63;

char input = 'z';

void setup() {
  Wire.begin(NODE_ADDRESS);
  Wire.onReceive(receiveEvent); // register event
  Serial.begin(9600); 
  Serial.print("nodeaddress: "); 
  Serial.println(NODE_ADDRESS);

  // stick 1
  pinMode(7, OUTPUT);
  pinMode(8, OUTPUT);
  // stick 2
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);
}

void loop() {
  delay(100);
  if(input == 'l' || input == 'r') {
    hitOneDrum(input);  
  }
}

void receiveEvent(int howMany) {
  while (Wire.available()) {
    char x = Wire.read();
    Serial.print("x= ");
    Serial.println(x);
    input = x;
  }
  

}


void hitOneDrum(char stick) {
  
  //Serial.println(stick);
  int pin1;
  int pin2;
  if(stick == 'l') {
    pin1 = 7;
    pin2 = 8;
      // Serial.println("foo1");
  } else if(stick == 'r'){
    pin1 = 9;
    pin2 = 10;
      // Serial.println("foo2");
  } 
    digitalWrite(pin1, HIGH);
    digitalWrite(pin2, LOW);
    delay(50);
    
    digitalWrite(pin1, LOW);
    digitalWrite(pin2, HIGH);
    delay(50);

    digitalWrite(pin1, HIGH);
    digitalWrite(pin2, LOW);
    input = 'z';
}

void millisDelay(int interval) {
  unsigned long previousMillis = millis();
  unsigned long currentMillis = millis();
  Serial.println(previousMillis);
  Serial.println(currentMillis);
  while(currentMillis - previousMillis < interval) {
    currentMillis = millis();
  }
}


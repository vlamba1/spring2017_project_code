const int WHOLE = 1000;
const int HALF = 500;
const int QUARTER = 250;
const int EIGHTH = 125;
const int SIXTEENTH = 63;

const int VOL1 = 43;
const int VOL2 = 85;
const int VOL3 = 128;
const int VOL4 = 170;
const int VOL5 = 213;
const int VOL6 = 255;

const int trigPin = 2;
const int echoPin = 4;

int inByte = 0;

void setup() {
  Serial.begin(9600);
  // stick 1
  pinMode(5, OUTPUT);
  pinMode(6, OUTPUT);
  // stick 2
  pinMode(9, OUTPUT);
  pinMode(10, OUTPUT);

}

void loop() {
  inByte = Serial.read();
    if(inByte == '1'){
      Serial.println("Playing song 1");
      song1();
      Serial.println("Done!");
    } else if(inByte == '2'){
      Serial.println("Playing song 2");
      song2();
      Serial.println("Done!");
    } else if(inByte == '3'){
      Serial.println("Playing song 3");
      song3();
      Serial.println("Done!");
    } else if(inByte == '4'){
      Serial.println("Playing song 4");
      song4();
      Serial.println("Done!");
    } else if(inByte == '5'){
      Serial.println("Playing song 5");
      song5();
      Serial.println("Done!");
    } else if(inByte == '6'){
      Serial.println("Playing song 6");
      song6();
      Serial.println("Done!");
    }else if(inByte == '7'){
      Serial.println("Playing song 7");
      song7();
      Serial.println("Done!");
    }
}

void hitOneDrum(char stick, int delayTime, int numTimes, int power) {
  int pin1;
  int pin2;
  if(strcmp(stick, 'l')) {
    pin1 = 5;
    pin2 = 6;
  } else if(strcmp(stick, 'r')){
    pin1 = 9;
    pin2 = 10;
  }
  for(int i=0; i<numTimes; i++) {
    analogWrite(pin1, power);
    analogWrite(pin2, 0);
    delay(50);
    
    analogWrite(pin1, 0);
    analogWrite(pin2, power);
    delay(50);

    analogWrite(pin1, power);
    analogWrite(pin2, 0);

    delay(delayTime);   

  }
  analogWrite(pin1, 0);
  analogWrite(pin2, 0);
}

void hitDrumAlt(int delayTime, int numTimes, int power) {
  for(int i=0; i<numTimes; i++) {
    analogWrite(9, power);
    analogWrite(10, 0);
    delay(50);
    analogWrite(9, 0);
    analogWrite(10, power);
    delay(50);
    analogWrite(9, power);
    analogWrite(10, 0);
    delay(delayTime);   

    analogWrite(5, power);
    analogWrite(6, 0);
    delay(50);
    analogWrite(5, 0);
    analogWrite(6, power);
    delay(50);
    analogWrite(5, power);
    analogWrite(6, 0);
    delay(delayTime);   

  }
  analogWrite(9, 0);
  analogWrite(10, 0);
  analogWrite(5, 0);
  analogWrite(6, 0);
}

void hitDrumBoth(int delayTime, int numTimes, int power) {
  for(int i=0; i<numTimes; i++) {
    analogWrite(5, power);
    analogWrite(6, 0);
    analogWrite(9, power);
    analogWrite(10, 0);
    delay(50);
    analogWrite(5, 0);
    analogWrite(6, power);
    analogWrite(9, 0);
    analogWrite(10, power);
    delay(50);
    analogWrite(5, power);
    analogWrite(6, 0);
    analogWrite(9, power);
    analogWrite(10, 0);   
    delay(delayTime);   

  }
  analogWrite(9, 0);
  analogWrite(10, 0);
  analogWrite(5, 0);
  analogWrite(6, 0);
}

/*
void hitOneDrumold(int delayTime, int numTimes, int power) {
  for(int i=0; i<numTimes; i++) {
    analogWrite(5, 0);
    analogWrite(6, power);
    
    analogWrite(9, power);
    analogWrite(10, 0);
    delay(delayTime);
    analogWrite(5, power);
    analogWrite(6, 0);
    
    analogWrite(9, 0);
    analogWrite(10, power);
    delay(delayTime);   
    analogWrite(9, 0);
    analogWrite(10, 0);
    analogWrite(5, 0);
    analogWrite(6, 0);
  }
  analogWrite(9, 0);
  analogWrite(10, 0);
  analogWrite(5, 0);
  analogWrite(6, 0);
}
*/

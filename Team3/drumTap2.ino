void song1() {
 
  hitDrumAlt(QUARTER, 2, VOL4);
  hitDrumBoth(HALF, 2, VOL6);
  delay(HALF);
  hitDrumAlt(QUARTER, 2, VOL4);
  hitDrumBoth(EIGHTH, 3, VOL5);
  delay(HALF);
  hitDrumAlt(EIGHTH, 4, VOL4);
  hitDrumBoth(WHOLE, 2, VOL6);
  hitDrumAlt(SIXTEENTH, 8, VOL4);
  
  hitDrumAlt(HALF, 1, VOL6);
  delay(WHOLE);
  hitDrumAlt(HALF, 1, VOL6);
  delay(WHOLE);
  hitDrumAlt(EIGHTH, 4, VOL6);
  hitDrumAlt(HALF, 1, VOL6);
  delay(QUARTER);
  hitDrumAlt(EIGHTH, 2, VOL4);
  hitDrumAlt(EIGHTH, 2, VOL5);
  hitDrumAlt(EIGHTH, 2, VOL5);
  hitDrumAlt(EIGHTH, 1, VOL6);

  delay(WHOLE);
  hitDrumAlt(QUARTER, 1, VOL6);
  delay(QUARTER);
  hitDrumAlt(QUARTER, 2, VOL6);
  delay(QUARTER);
  hitDrumAlt(QUARTER, 1, VOL6);
  delay(QUARTER);
  hitDrumAlt(QUARTER, 1, VOL6);
  hitDrumAlt(EIGHTH, 2, VOL6);
  delay(EIGHTH);

  hitDrumAlt(QUARTER, 2, VOL4);
  hitDrumAlt(HALF, 3, VOL6);
  delay(HALF);

  hitDrumAlt(QUARTER, 1, VOL6);
  delay(QUARTER);
  hitDrumAlt(QUARTER, 2, VOL6);
  delay(QUARTER);
  hitDrumAlt(QUARTER, 1, VOL6);
  delay(QUARTER);
  hitDrumAlt(QUARTER, 1, VOL6);
  hitDrumAlt(EIGHTH, 2, VOL6);
  delay(EIGHTH);
}  

void song2() {
   hitDrumAlt(QUARTER, 4, VOL4);
  hitDrumBoth(HALF, 2, VOL6);
  delay(WHOLE);
  hitDrumBoth(QUARTER, 4, VOL4);
  hitDrumBoth(HALF, 3, VOL5);
  delay(HALF);
  hitDrumBoth(EIGHTH, 8, VOL4);
  hitDrumBoth(WHOLE, 2, VOL6);
  hitDrumBoth(SIXTEENTH, 16, VOL4);
  delay(QUARTER);
  hitDrumAlt(HALF, 1, VOL6);
  hitDrumAlt(HALF, 1, VOL6);
  hitDrumAlt(EIGHTH, 8, VOL6);
  hitDrumAlt(HALF, 1, VOL6);
  delay(QUARTER);
  hitDrumAlt(EIGHTH, 4, VOL4);
  hitDrumAlt(EIGHTH, 4, VOL4);
  hitDrumAlt(EIGHTH, 4, VOL5);
  hitDrumAlt(EIGHTH, 2, VOL6);
  
  delay(WHOLE);
  hitDrumAlt(QUARTER, 1, VOL6);
  delay(QUARTER);
  hitDrumAlt(QUARTER, 2, VOL6);
  delay(QUARTER);
  hitDrumAlt(QUARTER, 1, VOL6);
  delay(QUARTER);
  hitDrumAlt(QUARTER, 1, VOL6);
  hitDrumAlt(EIGHTH, 2, VOL6);
  delay(EIGHTH);

  hitDrumAlt(QUARTER, 2, VOL4);
  hitDrumAlt(HALF, 3, VOL6);
  delay(HALF);
  hitDrumAlt(QUARTER, 2, VOL4);
  hitDrumAlt(HALF, 2, VOL5);
  delay(WHOLE);
  hitDrumAlt(EIGHTH, 4, VOL4);
  hitDrumAlt(WHOLE, 1, VOL6);
  hitDrumAlt(SIXTEENTH, 8, VOL4);
}


void song3() {
    hitOneDrum("l",HALF,1,VOL4);
  delay(QUARTER);
  hitDrumAlt(QUARTER,4,VOL6);
  delay(QUARTER);
  hitOneDrum("r",QUARTER,1,VOL6);
  delay(QUARTER);
  hitOneDrum("r",QUARTER,1,VOL6);
  hitDrumAlt(SIXTEENTH,8,VOL6);
  hitOneDrum("l",QUARTER,1,VOL6);
  delay(WHOLE);
  hitOneDrum("l",HALF,1,VOL4);
  delay(QUARTER);
  hitDrumAlt(QUARTER,4,VOL6);
  delay(QUARTER);
  hitOneDrum("r",QUARTER,1,VOL6);
  delay(QUARTER);
  hitOneDrum("r",QUARTER,1,VOL6);
  hitDrumAlt(SIXTEENTH,8,VOL6);
  hitOneDrum("l",QUARTER,1,VOL6);
  delay(WHOLE);
  hitDrumAlt(HALF,4,VOL4);
  hitDrumAlt(SIXTEENTH,2,VOL6);
  delay(EIGHTH);
  hitDrumAlt(HALF,4,VOL4);
  hitDrumAlt(SIXTEENTH,2,VOL6);
  delay(EIGHTH);
  hitOneDrum("l",EIGHTH,1,VOL4);
  hitOneDrum("r",EIGHTH,1,VOL4);
  hitOneDrum("l",EIGHTH,1,VOL4);
  delay(EIGHTH);
  hitOneDrum("l",EIGHTH,1,VOL6);
  hitOneDrum("r",EIGHTH,1,VOL6);
  hitOneDrum("l",EIGHTH,1,VOL6);
  delay(EIGHTH);
  hitDrumAlt(EIGHTH,4,VOL6);
  hitOneDrum("l",EIGHTH,1,VOL6);
  delay(EIGHTH);
  hitDrumBoth(WHOLE,1,VOL6);
  delay(QUARTER);
  hitDrumBoth(WHOLE,1,VOL6);
  delay(QUARTER);
  hitDrumAlt(SIXTEENTH,4,VOL6);
  delay(QUARTER);
  hitDrumAlt(SIXTEENTH,4,VOL6);
  hitDrumAlt(SIXTEENTH, 8, VOL6);


}

void song4() {
  long duration;
  long inches = 20;
  while(inches > 10) {
    pinMode(trigPin, OUTPUT);
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
  
    pinMode(echoPin, INPUT);
    duration = pulseIn(echoPin, HIGH);
  
    // convert the time into a distance
    inches = microsecondsToInches(duration);
    
    Serial.print(inches);
    Serial.print("in, ");
    Serial.println();
    delay(100);
  }
  song1();
}
void song5() {
  long duration;
  long inches = 20;
  while(inches > 10) {
    pinMode(trigPin, OUTPUT);
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
  
    pinMode(echoPin, INPUT);
    duration = pulseIn(echoPin, HIGH);
  
    // convert the time into a distance
    inches = microsecondsToInches(duration);
    
    Serial.print(inches);
    Serial.print("in, ");
    Serial.println();
    delay(100);
  }
  song2();
}
void song6() {
  long duration;
  long inches = 20;
  while(inches > 10) {
    pinMode(trigPin, OUTPUT);
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
  
    pinMode(echoPin, INPUT);
    duration = pulseIn(echoPin, HIGH);
  
    // convert the time into a distance
    inches = microsecondsToInches(duration);
    
    Serial.print(inches);
    Serial.print("in, ");
    Serial.println();
    delay(100);
  }
  song3();
}

void song7() {
  long duration;
  long inches = 20;
  while(inches > 5) {
    pinMode(trigPin, OUTPUT);
    digitalWrite(trigPin, LOW);
    delayMicroseconds(2);
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
  
    pinMode(echoPin, INPUT);
    duration = pulseIn(echoPin, HIGH);
  
    // convert the time into a distance
    inches = microsecondsToInches(duration);
    
    Serial.print(inches);
    Serial.print("in, ");
    Serial.println();
  
  
      digitalWrite(5, HIGH);
      digitalWrite(6, LOW);
      digitalWrite(9, HIGH);
      digitalWrite(10, LOW);
      delay(50);
      digitalWrite(5, LOW);
      digitalWrite(6, HIGH);
      digitalWrite(9, LOW);
      digitalWrite(10, HIGH);
      delay(50);
      digitalWrite(5, HIGH);
      digitalWrite(6, LOW);
      digitalWrite(9, HIGH);
      digitalWrite(10, LOW);   
      delay(inches*10);   
  }
}

long microsecondsToInches(long microseconds)
{
  return microseconds / 74 / 2;
}

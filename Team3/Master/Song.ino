
void song1() {
  char stick = 'l';
  for(int i=0; i<2; i++) {
    slave(1, 'l');
    delay(H);
    slave(1, 'r');
    delay(H);
    slave(1, 'l');
    delay(H);
    slave(1, 'r');
    delay(H);
    
    slave(1, 'l');
    delay(H);
    slave(2, 'l');
    delay(H);
    slave(3, 'l');
    delay(H);
    slave(4, 'l');
    delay(H);
    slave(5, 'l');
    delay(W);
    
    slave(1, 'l');
    delay(Q);
    slave(1, 'l');
    delay(Q);
    slave(1, 'l');
    delay(Q);
    slave(1, 'l');
    delay(W);
  }

  for(int i=0; i<2; i++) {
    slave(6, 'l');
    delay(Q);
    slave(6, 'l');
    delay(Q);
    slave(6, 'l');
    delay(Q);
    slave(6, 'l');
    delay(Q);
  }

    slave(5, 'l');
    delay(Q);
    slave(5, 'l');
    delay(W);
    slave(5, 'l');
    delay(Q);
    slave(5, 'l');
    delay(Q);

    slave(7, 'l');
    delay(Q);
  slave(7, 'l');
    delay(Q);
  slave(7, 'l');
    delay(Q);
  slave(7, 'l');
    delay(Q);
  slave(7, 'l');
    delay(Q);


  slave(2, 'l');
  delay(Q);
  
  slave(3, 'l');
  delay(Q);
  slave(3, 'r');
  delay(Q);
  slave(3, 'l');
  delay(Q);
  slave(3, 'r');
  delay(Q);
  
  slave(4, 'l');
  delay(Q);
  slave(4, 'r');
  delay(Q);
  slave(4, 'l');
  delay(Q);
  slave(4, 'r');
  delay(Q);
  
  slave(5, 'l');
  delay(Q);
  slave(5, 'l');
  delay(Q);
  slave(5, 'l');
  delay(Q);
  slave(5, 'l');
  delay(Q);
  delay(W);

  for(int i=0; i<4; i++) {
    slave(1, 'l');
    delay(Q);
    slave(1, 'l');
    delay(Q);
    slave(1, 'l');
    slave(5, 'l');
    delay(Q);
    slave(1, 'l');
    delay(Q);
  }
  
  
  for(int i=0; i<4; i++) {
    slave(1, 'l');
    slave(7, 'l');
    delay(Q);
    slave(1, 'l');
    delay(Q);
    slave(1, 'l');
    slave(5, 'l');
    slave(7, 'l');
    delay(Q);
    slave(1, 'l');
    delay(Q);
  }
  
  for(int i=0; i<6; i++) {
    slave(5, 'l');
    slave(7, 'l');
    delay(Q);
    slave(5, 'l');
    slave(7, 'l');
    slave(2, 'l');
    delay(Q);
    slave(5, 'l');
    slave(3, 'l');
    slave(7, 'l');
    delay(Q);
    slave(5, 'l');
    delay(Q);
  }
  
  
  for(int i=0; i<4; i++) {
    slave(1, 'l');
    slave(7, 'l');
    delay(Q);
    slave(1, 'l');
    delay(Q);
    slave(1, 'l');
    slave(5, 'l');
    slave(7, 'l');
    delay(Q);
    slave(1, 'l');
    delay(Q);
  }
  
  
  /*
  hitDrum(1, 'l', 4, W);
  //delay(H);
  hitDrum(1, 'l', 4, H);
  delay(H);
  hitDrum(1, 'l', 4, Q);
  delay(H);
  //hitDrum(2, 'l', 4, E);
  delay(H);
  hitDrum(1, 'l', 4, S);
  */
}



void hitDrum(int port, char stick, int numTimes, int delayTime) {
  for(int i=0; i<numTimes; i++) {
    slave(port, stick);
    slave(port+1, stick);
    millisDelay(delayTime);
  }
}


void millisDelay(int interval) {
  unsigned long previous = millis();
  unsigned long current = millis();
  while(current-previous < interval) {
    current = millis();
  }
}
